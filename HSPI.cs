using System;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

using HomeSeerAPI;
using Scheduler;
using HSPI_AKTemplate;


namespace HSPI_AKExample
{
    using HSDevice = Scheduler.Classes.DeviceClass;
    using DeviceDictHS = Dictionary<int, Scheduler.Classes.DeviceClass>;

    public class HSPI : HspiBase2
    {
        #region Variables

        /// <summary> Definition of the configuration web page </summary>
        //private HSPI_Config configPage;
        private StatusPage statusPage;



        public Settings settings { get; protected set; }

        #endregion Variables

        #region Construction Etc.

        public void SaveSettings()
        {
            settings.Save();
        }

        protected override string GetName()
        {
            return "AK HSPI Template";
        }


        /// <summary>
        /// Ctor
        /// </summary>
        public HSPI()
        {
            controller = null;
            //settings = new Settings(IniFile, HS);

            Utils.PluginName = GetName();
        }

        /// <summary>
        /// Initialize the plugin and associated hardware/software, start any threads
        /// </summary>
        /// <param name="port">The COM port for the plugin if required.</param>
        /// <returns>Warning message or empty for success.</returns>
        public override string InitIO(string port)
        {
            // Connect() should call it, but doesn't hurt to call again
            Connected(true);

            // First time create Controller
            if (controller == null)
                controller = new Controller(this);

            // After reconnect - just update, don't recreate
            controller.Update(settings.InitIOinThread);

            // Create and register the web pages

            statusPage = new StatusPage(this);

            //configPage = new HSPI_Config(this);

            // Register help page
            //RegisterWebPageDesc(Name + " Help", "AKSmartDevice/AKSmartDeviceVer1.htm", "Help1", helplink: true);
            //RegisterWebPageDesc("Help", "/AKSmartDevice/AKSmartDeviceVer1.htm", "Help2", helplink: false);
            //RegisterWebPageDesc("Youtube Video", "http://www.youtube.com/watch?v=mmyrx8kXnWY", "Help2");

            // Note: SetIOMulti() is called ONLY for devices owned by this plugin
            // To recive value update for device from another plugin need to
            // RegisterEventCB(HSEvent.VALUE_CHANGE), then HSEvent() is called for EVERY device
            Callback.RegisterEventCB(Enums.HSEvent.VALUE_CHANGE, GetName(), InstanceFriendlyName());
            Callback.RegisterEventCB(Enums.HSEvent.CONFIG_CHANGE, GetName(), InstanceFriendlyName());

            //AppDomain currentDomain = AppDomain.CurrentDomain;
            //currentDomain.AssemblyResolve += new ResolveEventHandler(Utils.LoadFromSameFolder);

            return base.InitIO(null);
        }


        /// <summary>
        /// When HSPIBase.Connect succseeds it calls this virtual function to inform plugin
        /// </summary>
        protected override void Connected(bool isconnected)
        {
            Console.WriteLine($"HSPI Connected({isconnected})");

            if (isconnected)
            {
                // Not sure if this exactly clean?
                Utils.Hs = HS;

                settings = new Settings(IniFile, HS);

                //controller.CheckSavedValues();
            }
            else
            {
                Utils.Hs = null;
                if(controller!=null)
                    controller.HS = null;
            }
        }

        /// <summary>
        ///     Called when HomeSeer is not longer using the plugin.
        ///     This call will be made if a user disables a plugin from the interfaces configuration page
        ///     and when HomeSeer is shut down.
        /// </summary>
        public override void ShutdownIO()
        {
            // let our console wrapper know we are finished
            base.ShutdownIO();
        }

        #endregion Construction

        #region Capabilities and Status

        public override IPlugInAPI.strInterfaceStatus InterfaceStatus()
        {
            return new IPlugInAPI.strInterfaceStatus
            {
                intStatus = IPlugInAPI.enumInterfaceStatus.OK
            };
        }

        /// <summary>
        /// Plugin licensing mode: 
        /// 1 = plugin is not licensed, 
        /// 2 = plugin is licensed and user must purchase a license but there is a 30-day trial.
        /// </summary>
        /// <returns>System.Int32</returns>
        public override int AccessLevel()
        {
            #if DEBUG
            return 1;
            #else
            return 2;
            #endif
        }

 
        /// <summary>
        /// Indicate if the plugin supports the ability to add devices through the Add Device link on the device utility page.
        /// If <c>true</c>, a tab appears on the add device page that allows the user to configure specific options for the new device.
        /// </summary>
        public override bool SupportsAddDevice()
        {
            return true;
        }

        /// <summary>
        /// Indicate if the plugin allows for configuration of the devices via the device utility page.
        /// This will allow you to generate some HTML controls that will be displayed to the user for modifying the device.
        /// </summary>
        public override bool SupportsConfigDevice()
        {
            return true;
        }

        /// <summary> Indicate if the plugin manages all devices in the system. </summary>
        public override bool SupportsConfigDeviceAll()
        {
            return true;
        }

        /// <summary>
        /// Indicate if the plugin supports multiple instances.  
        /// The plugin may be launched multiple times and will be passed a unique instance name as a command line parameter to the Main function.
        /// </summary>
        public override bool SupportsMultipleInstances()
        {
            return false;
        }

        /// <summary> Indicate if plugin supports multiple instances using a single executable.</summary>
        public override bool SupportsMultipleInstancesSingleEXE()
        {
            return false;
        }

#endregion Capabilities and Status

        #region Devices

        #endregion Devices

        #region Support functions



        /// <summary>
        /// Create a new device and set the names for the status display.
        /// </summary>
        /// <param name="refId">The device reference identifier for HomeSeer.</param>
        /// <param name="name">The name for the device.</param>
        /// <returns>Scheduler.Classes.DeviceClass.</returns>
        //private Scheduler.Classes.DeviceClass CreateDevice(out int refId, string name = "HikVision Camera")
        //{
        //    Scheduler.Classes.DeviceClass dv = null;
        //    refId = HS.NewDeviceRef(name);
        //    if (refId > 0)
        //    {
        //        dv = (Scheduler.Classes.DeviceClass)HS.GetDeviceByRef(refId);
        //        dv.set_Address(HS, "Camera" + refId);
        //        dv.set_Device_Type_String(HS, "HikVision Camera Alarm");
        //        DeviceTypeInfo_m.DeviceTypeInfo DT = new DeviceTypeInfo_m.DeviceTypeInfo();
        //        DT.Device_API = DeviceTypeInfo_m.DeviceTypeInfo.eDeviceAPI.Security;
        //        DT.Device_Type = (int)DeviceTypeInfo_m.DeviceTypeInfo.eDeviceType_Security.Zone_Interior;
        //        dv.set_DeviceType_Set(HS, DT);
        //        dv.set_Interface(HS, GetName());
        //        dv.set_InterfaceInstance(HS, "");
        //        dv.set_Last_Change(HS, DateTime.Now);
        //        dv.set_Location(HS, "Camera"); // room
        //        dv.set_Location2(HS, "HikVision"); // floor
        //
        //        VSVGPairs.VSPair Pair = new VSVGPairs.VSPair(HomeSeerAPI.ePairStatusControl.Status);
        //        Pair.PairType = VSVGPairs.VSVGPairType.SingleValue;
        //        Pair.Value = -1;
        //        Pair.Status = "Unknown";
        //        Default_VS_Pairs_AddUpdateUtil(refId, Pair);
        //
        //        Pair = new VSVGPairs.VSPair(HomeSeerAPI.ePairStatusControl.Status);
        //        Pair.PairType = VSVGPairs.VSVGPairType.SingleValue;
        //        Pair.Value = 0;
        //        Pair.Status = "No Motion";
        //        Default_VS_Pairs_AddUpdateUtil(refId, Pair);
        //
        //        Pair = new VSVGPairs.VSPair(HomeSeerAPI.ePairStatusControl.Status);
        //        Pair.PairType = VSVGPairs.VSVGPairType.SingleValue;
        //        Pair.Value = 1;
        //        Pair.Status = "Motion";
        //        Default_VS_Pairs_AddUpdateUtil(refId, Pair);
        //
        //        dv.MISC_Set(HS, Enums.dvMISC.STATUS_ONLY);
        //        dv.MISC_Set(HS, Enums.dvMISC.SHOW_VALUES);
        //        dv.set_Status_Support(HS, true);
        //    }
        //
        //    return dv;
        //}

        /// <summary>
        /// Add the protected, default VS/VG pairs WITHOUT overwriting any user added pairs unless absolutely necessary (because they conflict).
        /// </summary>
        /// <param name="refId">The device reference identifier for HomeSeer.</param>
        /// <param name="Pair">The value/status pair.</param>
        //private void Default_VS_Pairs_AddUpdateUtil(int refId, VSVGPairs.VSPair Pair)
        //{
        //    if ((Pair == null) || (refId < 1) || (!HS.DeviceExistsRef(refId)))
        //        return;
        //
        //    try
        //    {
        //        VSVGPairs.VSPair Existing = HS.DeviceVSP_Get(refId, Pair.Value, Pair.ControlStatus);
        //        if (Existing != null)
        //        {
        //            // This is unprotected, so it is a user's value/ status pair.
        //            if ((Existing.ControlStatus == HomeSeerAPI.ePairStatusControl.Both) && (Pair.ControlStatus != HomeSeerAPI.ePairStatusControl.Both))
        //            {
        //                // The existing one is for BOTH, so try changing it to the opposite of what we are adding and then add it.
        //                if (Pair.ControlStatus == HomeSeerAPI.ePairStatusControl.Status)
        //                {
        //                    if (!HS.DeviceVSP_ChangePair(refId, Existing, HomeSeerAPI.ePairStatusControl.Control))
        //                    {
        //                        HS.DeviceVSP_ClearBoth(refId, Pair.Value);
        //                        HS.DeviceVSP_AddPair(refId, Pair);
        //                    }
        //                    else
        //                        HS.DeviceVSP_AddPair(refId, Pair);
        //                }
        //                else
        //                {
        //                    if (!HS.DeviceVSP_ChangePair(refId, Existing, HomeSeerAPI.ePairStatusControl.Status))
        //                    {
        //                        HS.DeviceVSP_ClearBoth(refId, Pair.Value);
        //                        HS.DeviceVSP_AddPair(refId, Pair);
        //                    }
        //                    else
        //                        HS.DeviceVSP_AddPair(refId, Pair);
        //                }
        //            }
        //            else if (Existing.ControlStatus == HomeSeerAPI.ePairStatusControl.Control)
        //            {
        //                // There is an existing one that is STATUS or CONTROL - remove it if ours is protected.
        //                HS.DeviceVSP_ClearControl(refId, Pair.Value);
        //                HS.DeviceVSP_AddPair(refId, Pair);
        //            }
        //            else if (Existing.ControlStatus == HomeSeerAPI.ePairStatusControl.Status)
        //            {
        //                // There is an existing one that is STATUS or CONTROL - remove it if ours is protected.
        //                HS.DeviceVSP_ClearStatus(refId, Pair.Value);
        //                HS.DeviceVSP_AddPair(refId, Pair);
        //            }
        //        }
        //        else
        //        {
        //            // There is not a pair existing, so just add it.
        //            HS.DeviceVSP_AddPair(refId, Pair);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        #endregion Support functions

    }
}