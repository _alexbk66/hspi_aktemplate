﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using HomeSeerAPI;
using HSPI_AKTemplate;
using Scheduler;
using static Scheduler.clsJQuery;



namespace HSPI_AKExample
{

    #region Action

    public enum Actions
    {
        screenOff = 1,
        Action_2 = 2
    }


    [Serializable]
    public class ScreenOffAction : ActionData
    {
        public bool someBoolValue = false;

        public ScreenOffAction(ControllerBase controller = null)
            : base((int)Actions.screenOff, "Screen: Off", controller)
        {
        }


        /// <summary>
        /// Execute the Action
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override bool Execute(IPlugInAPI.strTrigActInfo actionInfo)
        {
            (controller as Controller).ScreenOff();
            return true;
        }

        /// <summary>
        /// Build Action UI
        /// </summary>
        /// <param name="uniqueControlId"></param>
        /// <param name="actInfo">strTrigActInfo</param>
        /// <returns></returns>
        public override string BuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo actInfo)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" Select:");

            var chkBox = new jqCheckBox("chck" + uniqueControlId, "some text", "events", true, true)
            {
                @checked = someBoolValue,
            };

            sb.Append(chkBox.Build());

            return sb.ToString();
        }

        /// <summary>
        /// Process user selection from event Action configuration
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override IPlugInAPI.strMultiReturn ProcessPostUI(NameValueCollection postData,
                IPlugInAPI.strTrigActInfo actionInfo)
        {
            string res = "";
            foreach (string key in postData.AllKeys)
            {
                string val = postData[key];

                // Remove uniqueControlId
                string[] split = key.Split('_');
                string key1 = split[0];
                //res = $"{key1}: {val}";
                switch (key1)
                {
                    case "chck":
                        someBoolValue = (bool)Utils.StrToBool(val);
                        break;
                }
            }

            // Here the new ActionData instance is created via Serialization
            return MakeReturn(actionInfo, error: res);
        }


        /// <summary>
        /// After the action has been configured, this function is called in your plugin
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override string FormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            if (!get_Configured())
                return null;

            return $"Turn Screen Off, checkbox selected '{someBoolValue}' </BR> (UID {actionInfo.UID}, Ev {actionInfo.evRef})";
        }


        /// <summary>
        /// Return TRUE if the given action is configured properly.
        /// There may be times when a user can select invalid selections for the action and 
        /// in this case you would return FALSE so HomeSeer will not allow the action to be saved.
        /// </summary>
        public override bool get_Configured()
        {
            return someBoolValue == true;
        }

    }


    #endregion Action


    #region Trigger

    public enum Triggers
    {
        SomeTrigger = 1
    }

    public enum SubTriggers
    {
        SubUnset = -1,
        SubNotUsed = 0,
        SomeSubTrigger1 = 1,
        SomeSubTrigger2 = 2,
    }



    [Serializable]
    public class SomeTriggerData : TriggerData
    {
        public bool someBoolValue = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="controller"></param>
        /// <param name="RegisterCallback"></param>
        public SomeTriggerData( bool condition = false,
                                ControllerBase controller = null,
                                bool RegisterCallback = true)
            : base((int)Triggers.SomeTrigger, "Some Trigger Example", condition, controller, RegisterCallback)
        {
            //subbtriggers.Add(new TriggerData());// TANumber/SubTANumber is 1 based
            //subbtriggers.Add(new SubEventTrigger(controller));
            //subbtriggers.Add(new SubEventReminder(controller));
            //subbtriggers.Add(new SubEventEndTrigger(controller));
        }


        /// <summary>
        /// Build Trigger UI
        /// </summary>
        /// <param name="uniqueControlId"></param>
        /// <param name="triggerInfo"></param>
        /// <returns></returns>
        public override string BuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo triggerInfo)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append(" Select:");

            var chkBox = new jqCheckBox("chck" + uniqueControlId, "some text", "events", true, true)
            {
                @checked = someBoolValue,
            };

            sb.Append(chkBox.Build());


            return sb.ToString();
        }


        /// <summary>
        /// After the trigger has been configured, this function is called in your plugin
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override string FormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            if (!get_Configured())
                return null;

            return $"If something happens, checkbox selected '{someBoolValue}' </BR> (UID {actionInfo.UID}, Ev {actionInfo.evRef})";
        }

        /// <summary>
        /// Process user selection from event Trigger configuration
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override IPlugInAPI.strMultiReturn ProcessPostUI(NameValueCollection postData,
                IPlugInAPI.strTrigActInfo actionInfo)
        {
            string res = "";
            foreach (string key in postData.AllKeys)
            {
                string val = postData[key];

                // Remove uniqueControlId
                string[] split = key.Split('_');
                string key1 = split[0];
                //res = $"{key1}: {val}";
                switch (key1)
                {
                    case "chck":
                        someBoolValue = (bool)Utils.StrToBool(val);
                        break;
                }
            }

            // Here the new ActionData instance is created via Serialization
            return MakeReturn(actionInfo, error: res);
        }


        public override bool get_Configured()
        {
            return someBoolValue == true;
        }


        /// <summary>
        /// Check if Trigger should be fired
        /// Before fire set global variables
        /// </summary>
        /// <param name="source">CalendarRootBaseDevice</param>
        /// <param name="data">Event</param>
        /// <returns></returns>
        public override bool TriggerTrue(object source, object data)
        {
            if (!get_Configured())
                return false;

            //CalendarRootBaseDevice calDevice = source as CalendarRootBaseDevice;
            double value = (double)data;
            
            bool trigger = (value == 0);

            return trigger;
        }

    }




    #endregion Trigger
}
