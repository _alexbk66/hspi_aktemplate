﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HSPI_AKTemplate
{
    public partial class PageBuilder
    {
        static readonly Assembly SystemAssembly = typeof(object).Assembly;

        static bool IsSystemType(object o) => o == null || o is Enum || o.GetType().Assembly == SystemAssembly;

        /// <summary>
        /// Check is object is an array/list (but not string)
        /// </summary>
        /// <param name="o">object</param>
        /// <param name="not_system">Return false for array of system types</param>
        /// <returns></returns>
        static bool IsCollection(object o, bool not_system = true)
        {
            if (o == null)
                return false;

            Type type = o.GetType();
            IEnumerable coll1 = o as IEnumerable;
            bool is_collection = (coll1 != null) && !(o is String);
            if (is_collection && not_system)
            {
                if (o as IEnumerable<int> != null || o as IEnumerable<double> != null || o as IEnumerable<string> != null)
                    return false;

                IEnumerable<object> coll2 = o as IEnumerable<object>;
                if (coll2 == null)
                    return false;

                object val0 = coll2.FirstOrDefault();
                is_collection = !IsSystemType(val0);
            }
            return is_collection;
        }

        /// <summary>
        /// Helper for TableObjectProperties 
        /// Show objects properties or fields, depending on T type
        /// </summary>
        /// <typeparam name="T">PropertyInfo or FieldInfo</typeparam>
        /// <param name="o">Object which properties/fields to show</param>
        /// <param name="table">TableBuilder</param>
        /// <param name="fields">Show fields?</param>
        /// <param name="props">Show properties?</param>
        /// <param name="parent"></param>
        void ShowValues<T>(object o, TableBuilder table, bool fields, bool props, string parent = null) where T : MemberInfo
        {
            try
            {
                _ShowValues<T>(o, table, fields, props, parent);
            }
            catch (Exception ex)
            {
                Utils.Log($"ShowValues {ex.Message} ({o})", logLevel: Utils.LogType.Error);
            }
        }


        /// <summary>
        /// Helper for TableObjectProperties 
        /// Show objects properties or fields, depending on T type
        /// </summary>
        /// <typeparam name="T">PropertyInfo or FieldInfo</typeparam>
        /// <param name="o">Object which properties/fields to show</param>
        /// <param name="table">TableBuilder</param>
        /// <param name="fields">Show fields?</param>
        /// <param name="props">Show properties?</param>
        /// <param name="parent">Name of parent object</param>
        void _ShowValues<T>(object o, TableBuilder table, bool fields, bool props, string parent = null) where T : MemberInfo
        {
            if (o == null)
                return;

            //Console.WriteLine($"{o} ({parent})"); // TEMP

            if (IsCollection(o) || o is String)
            {
                PropertyRow(table, parent, o);
                return;
            }

            // objects which are classes or collections (Lists/Arrays), to call nested TableObjectProperties
            Dictionary<string, object> classes = new Dictionary<string, object>();

            // T is PropertyInfo or FieldInfo
            bool isProps = (typeof(T) == typeof(PropertyInfo));

            Type type = o.GetType();

            MemberInfo[] arr = isProps ? (MemberInfo[])type.GetProperties() : (MemberInfo[])type.GetFields();

            // Enumerate properties
            foreach (T prop in arr)
            {
                object val = null;
                if (isProps)
                    val = (prop as PropertyInfo).GetValue(o);
                else
                    val = (prop as FieldInfo).GetValue(o);

                // Check if it's a List/Array or a class object
                if (IsCollection(val) || !IsSystemType(val))
                {
                    classes[prop.Name] = val;
                }
                else
                {
                    PropertyRow(table, prop.Name, val);
                }
            }

            // Now show all found class type members and Lists/Arrays
            foreach (KeyValuePair<string, object> pair in classes)
            {
                int index = 0;
                object o1 = pair.Value;

                string parent1 = $"{parent}.{pair.Key}";

                if (IsCollection(o1))
                {
                    foreach (object o2 in (o1 as IEnumerable<object>))
                        TableObjectProperties(o2, table, fields, props, parent1, index++);
                }
                else
                {
                    TableObjectProperties(o1, table, fields, props, parent1);
                }
            }
        }


        /// <summary>
        /// Show objects properties/fields in a table
        /// </summary>
        /// <param name="o"></param>
        /// <param name="table"></param>
        /// <param name="fields"></param>
        /// <param name="props"></param>
        /// <param name="parent"></param>
        /// <param name="index"></param>
        public void TableObjectProperties(object o, TableBuilder table, bool fields, bool props, string parent = null, int index = -1)
        {
            if (o == null)
                return;

            string name = $"{o}";
            string[] split = name.Split('+');
            if (split.Length > 1)
                name = split[split.Length - 1];

            if (!String.IsNullOrEmpty(parent))
                name = $"{parent}.{name}";

            if (index >= 0)
                name = $"{name}[{index}]";

            string[] hdr = { $"'{name}'", "Value" };
            table.AddRowHeader(hdr);

            if (props)
            {
                ShowValues<PropertyInfo>(o, table, fields, props, name);
            }

            if (fields)
            {
                if (props)
                    table.AddBlankRow();

                ShowValues<FieldInfo>(o, table, fields, props, name);
            }
        }


        /// <summary>
        /// Add row with Name and Value
        /// </summary>
        /// <param name="table"></param>
        /// <param name="Name"></param>
        /// <param name="val"></param>
        private void PropertyRow(TableBuilder table, string Name, object val)
        {
            PropertyRow(table.AddRow(), Name, val);
        }

        /// <summary>
        /// Add row with Name and Value
        /// </summary>
        /// <param name="row"></param>
        /// <param name="Name"></param>
        /// <param name="val"></param>
        private void PropertyRow(TableBuilder.TableRow row, string Name, object val)
        {
            row.AddCell(Name);

            // Convert Array/List to string
            if (val is IEnumerable && !(val is String))
            {
                string s = "";
                foreach (var v in (val as IEnumerable))
                {
                    s += (v != null ? v.ToString() : "") + ",";
                }
                val = s;
            }

            string sval = val != null ? val.ToString() : "";
            if (sval.Length > 125)
                sval = sval.Substring(0, 125) + " ...";

            row.AddCell(sval);
        }
    }
}