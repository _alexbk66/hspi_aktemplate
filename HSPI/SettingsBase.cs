﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HomeSeerAPI;

namespace HSPI_AKTemplate
{
    public class MySettingsBase
    {
        public IHSApplication Hs { get; protected set; }

        public string IniFile { get; protected set; }


        public MySettingsBase(string IniFile, IHSApplication Hs)
        {
            this.IniFile = IniFile;
            this.Hs = Hs;
            _Load();
        }

        private string _location;
        public string Location
        {
            get => _location;
            set
            {
                _location = value;
                SaveVal("Location", _location);
            }
        }

        private string _location2;
        public string Location2
        {
            get => _location2;
            set
            {
                _location2 = value;
                SaveVal("Location2", _location2);
            }
        }

        //private string _selectedDeviceRef;
        //public string SelectedDeviceRef
        //{
        //    get => _selectedDeviceRef;
        //    set
        //    {
        //        _selectedDeviceRef = value;
        //        SaveVal("SelectedDeviceRef", _selectedDeviceRef);
        //    }
        //}

        private bool _debugLog;
        public bool DebugLog
        {
            get => _debugLog;
            set
            {
                _debugLog = value;
                SaveVal("DebugLog", _debugLog);
            }
        }

        private static bool _showPEDsInGetURL;
        public static bool ShowPEDsInGetURL
        {
            get => _showPEDsInGetURL;
            set
            {
                _showPEDsInGetURL = value;
                //SaveVal("ShowPEDsInGetURL", _showPEDsInGetURL);
            }
        }


        /// <summary>
        /// Call UpdateConfiguration in main or new thread
        /// </summary>
        private bool _InitIOinThread;
        public bool InitIOinThread
        {
            get => _InitIOinThread;
            set
            {
                _InitIOinThread = value;
                SaveVal("InitIOinThread", _InitIOinThread);
            }
        }

        /// <summary>
        /// Display tables inside SliderTab
        /// </summary>
        private bool _InSlider;
        public bool InSlider
        {
            get => _InSlider;
            set
            {
                _InSlider = value;
                SaveVal("InSlider", _InSlider);
            }
        }

        /// <summary>
        /// Prevent calling SaveVal while calling Load()
        /// </summary>
        private bool loading = false;

        public virtual void _Load()
        {
            loading = true;

            InSlider = LoadVal(nameof(InSlider), true);
            InitIOinThread = LoadVal(nameof(InitIOinThread), true);
            DebugLog = LoadVal(nameof(DebugLog), false);
            ShowPEDsInGetURL = LoadVal(nameof(ShowPEDsInGetURL), true);
            Location = LoadVal(nameof(Location));
            Location2 = LoadVal(nameof(Location2));
            //SelectedDeviceRef = LoadVal(nameof(SelectedDeviceRef));

            // Virtual
            Load();

            loading = false;
        }

        public virtual void Load() { }

        public virtual void Save() { }

        public virtual void _Save()
        {
            SaveVal(nameof(InSlider), InSlider);
            SaveVal(nameof(InitIOinThread), InitIOinThread);
            SaveVal(nameof(DebugLog), DebugLog);
            SaveVal(nameof(ShowPEDsInGetURL), ShowPEDsInGetURL);
            SaveVal(nameof(Location), Location);
            SaveVal(nameof(Location2), Location2);
            //SaveVal(nameof(SelectedDeviceRef), SelectedDeviceRef);

            // Virtual
            Save();
        }


        public T LoadVal<T>(string key, T def = default(T), string section = "Settings", string fName = null)
        {
            try
            {
                return (T)Convert.ChangeType(LoadVal(key, $"{def}", section, fName), typeof(T));
            }
            catch(Exception ex)
            {
                Console.WriteLine($"LoadVal<{typeof(T)}>({key}): {ex.Message}");
            }
            return default(T);
        }

        public string LoadVal(string key, string def = "", string section = "Settings", string fName = null)
        {
            if (fName == null) fName = IniFile;
            return Hs.GetINISetting(section, key, def, fName);
        }

        public void SaveVal(string key, object value, string section = "Settings", string fName = null)
        {
            if (!loading)
            {
                if (fName == null) fName = IniFile;
                Hs.SaveINISetting(section, key, value.ToString(), fName);
            }
        }
    }

}
