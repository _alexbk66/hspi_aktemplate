﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using HomeSeerAPI;
using Scheduler;
using static Scheduler.clsJQuery;

namespace HSPI_AKTemplate
{
    public partial class PageBuilder
    {
        #region Button

        /// <summary>
        /// Build a button for a web page.
        /// </summary>
        /// <param name="Text">The text on the button.</param>
        /// <param name="Name">The name used to create the references for the button.</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].</param>
        /// <returns>The text to insert in the web page to create the button.</returns>
        protected string BuildButton(string Text, string Name, bool Enabled = true)
        {
            return "<div id='" + Name + "_div'>" + FormButton(Name, Text, Enabled: Enabled) + "</div>";
        }

        /// <summary>
        /// Update a button on a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Text">The text on the button.</param>
        /// <param name="Name">The name used to create the references for the button.</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].</param>
        protected void UpdateButton(string Text, string Name, bool Enabled = true)
        {
            divToUpdate.Add(Name + "_div", FormButton(Name, Text, Enabled: Enabled));
        }

        /// <summary>
        /// Return the string required to create a web page button.
        /// </summary>
        protected string FormButton(string Name,
                                     string label = "Submit",
                                     bool SubmitForm = true,
                                     string ImagePathNormal = "",
                                     string ImagePathPressed = "",
                                     string ToolTip = "",
                                     bool Enabled = true,
                                     string Style = "",
                                     string className = "",
                                     string url = null,
                                     int width = 0,
                                     string pageName = null)
        {
            if (pageName == null)
                pageName = this.PageLink;

            jqButton b = new jqButton(Name, label, pageName, SubmitForm)
            {
                //id = id_prefix + Name,
                url = url,
                hyperlink = false,
                urlNewWindow = true,
                imagePathNormal = ImagePathNormal,
                toolTip = ToolTip,
                enabled = Enabled,
                style = Style,
                width = width
            };
            b.imagePathPressed = (ImagePathPressed != "") ? ImagePathPressed : b.imagePathNormal;

            string Button = b.Build();
            //Button.Replace("</button>\r\n", "</button>");

            // Replace "Submit" label
            Button = Button.Replace($"&{Name}=Submit", $"&{Name}={label}");

            return Button;
        }

        #endregion Button

        #region Label

        /// <summary>
        /// Build a label for a web page.
        /// </summary>
        /// <param name="Text">The text for the label.</param>
        /// <param name="Name">The name used to create the references for the label.</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].</param>
        /// <returns>The text to insert in the web page to create the label.</returns>
        protected string BuildLabel(string Name, string Msg = "")
        {
            return "<div id='" + Name + "_div'>" + FormLabel(Name, Msg) + "</div>";
        }

        /// <summary>
        /// Update a label on a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Text">The text for the label.</param>
        /// <param name="Name">The name used to create the references for the label.</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].</param>
        protected void UpdateLabel(string Name, string Msg = "")
        {
            divToUpdate.Add(Name + "_div", FormLabel(Name, Msg));
        }

        /// <summary>
        /// Return the string required to create a web page label.
        /// </summary>
        protected string FormLabel(string Name, string Message = "", bool Visible = true)
        {
            string Content;
            if (Visible)
                Content = Message + "<input id='" + Name + "' Name='" + Name + "' Type='hidden'>";
            else
                Content = "<input id='" + Name + "' Name='" + Name + "' Type='hidden' value='" + Message + "'>";
            return Content;
        }

        #endregion Label

        #region TextBox

        public static string FormTextBox_(string Name,
                                      string pageName,
                                      string DefaultText = "",
                                      string dialogCaption = "",
                                      string promptText = "",
                                      int size = 0,
                                      bool SubmitForm = true,
                                      string style = "")
        {
            var txtBox = new jqTextBox(Name, "text", DefaultText, pageName, size, SubmitForm)
            {
                dialogCaption = dialogCaption,
                promptText = promptText,
                style = style
            };
            return txtBox.Build();
        }

        protected string FormTextBox(string Name,
                                      string DefaultText = "",
                                      string dialogCaption = "",
                                      string promptText = "",
                                      int size = 0,
                                      bool SubmitForm = true,
                                      string style = "")
        {
            return FormTextBox_(Name, this.PageLink, DefaultText, dialogCaption, promptText, size, SubmitForm, style);
        }

        /// <summary>
        /// Build a text entry box for a web page.
        /// </summary>
        /// <param name="Text">The default text for the text box.</param>
        /// <param name="Name">The name used to create the references for the text box.</param>
        /// <param name="AllowEdit">if set to <c>true</c> allow the text to be edited.</param>
        /// <returns>The text to insert in the web page to create the text box.</returns>
        protected string BuildTextBox(string Name, string Text = "", bool AllowEdit = true)
        {
            return "<div id='" + Name + "_div'>" + HTMLTextBox(Name, Text, 20, AllowEdit) + "</div>";
        }

        /// <summary>
        /// Update a text box on a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Text">The text for the text box.</param>
        /// <param name="Name">The name used to create the references for the text box.</param>
        /// <param name="AllowEdit">if set to <c>true</c> allow the text to be edited.</param>
        protected void UpdateTextBox(string Name, string Text = "", bool AllowEdit = true)
        {
            divToUpdate.Add(Name + "_div", HTMLTextBox(Name, Text, 20, AllowEdit));
        }

        /// <summary>
        /// Return the string required to create a web page text box.
        /// </summary>
        protected string HTMLTextBox(string Name, string DefaultText, int Size, bool AllowEdit = true)
        {
            string Style = "";
            string sReadOnly = "";

            if (!AllowEdit)
            {
                Style = "color:#F5F5F5; background-color:#C0C0C0;";
                sReadOnly = "readonly='readonly'";
            }

            return $"<input type='text' id='{/*id_prefix + */Name}' style='{Style}' size='{Size}' name='{Name}' {sReadOnly} value='{DefaultText}'>";
        }

        #endregion TextBox

        #region CheckBox

        /// <summary>
        /// Build a check box for a web page.
        /// </summary>
        /// <param name="Name">The name used to create the references for the text box.</param>
        /// <param name="Checked">if set to <c>true</c> [checked].</param>
        /// <returns>The text to insert in the web page to create the check box.</returns>
        protected string BuildCheckBox(string Name,
                                        bool Checked = false,
                                        bool SubmitForm = true)
        {
            return "<div id='" + Name + "_div'>" + FormCheckBox(Name, Checked, SubmitForm: SubmitForm) + "</div>";
        }

        /// <summary>
        /// Update a check box on a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Name">The name used to create the references for the text box.</param>
        /// <param name="Checked">if set to <c>true</c> [checked].</param>
        protected void UpdateCheckBox(string Name, bool Checked = false)
        {
            divToUpdate.Add(Name + "_div", FormCheckBox(Name, Checked));
        }

        /// <summary>
        /// Return the string required to create a web page check box.
        /// </summary>
        protected string FormCheckBox(string Name,
                                       bool Checked = false,
                                       string label = "",
                                       bool AutoPostBack = true,
                                       bool SubmitForm = true)
        {
            UsesjqCheckBox = true;
            return FormCheckBox(Name, PageLink, Checked, label, AutoPostBack, SubmitForm);
        }

        public static string FormCheckBox(string Name,
                                       string pageName,
                                       bool Checked = false,
                                       string label = "",
                                       bool AutoPostBack = true,
                                       bool SubmitForm = true)
        {
            jqCheckBox cb = new jqCheckBox(Name, label, pageName, AutoPostBack, SubmitForm)
            {
                //id = id_prefix + Name,
                @checked = Checked
            };
            return cb.Build();
        }

        #endregion CheckBox

        #region ListBox

        /// <summary>
        /// Build a list box for a web page.
        /// </summary>
        /// <param name="Name">The name used to create the references for the list box.</param>
        /// <param name="Options">Data value pairs used to populate the list box.</param>
        /// <param name="Selected">Index of the item to be selected.</param>
        /// <param name="SelectedValue">Name of the value to be selected.</param>
        /// <param name="Width">Width of the list box</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].  Doesn't seem to work.</param>
        /// <returns>The text to insert in the web page to create the list box.</returns>
        protected string BuildListBox(string Name, ref MyPairList Options, int Selected = -1, string SelectedValue = "", int Width = 150, bool Enabled = true)
        {
            return "<div id='" + Name + "_div'>" + FormListBox(Name, ref Options, Selected, SelectedValue, Width, Enabled) + "</div>";
        }

        /// <summary>
        /// Update a list box for a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Name">The name used to create the references for the list box.</param>
        /// <param name="Options">Data value pairs used to populate the list box.</param>
        /// <param name="Selected">Index of the item to be selected.</param>
        /// <param name="SelectedValue">Name of the value to be selected.</param>
        /// <param name="Width">Width of the list box</param>
        /// <param name="Enabled">if set to <c>true</c> [enabled].  Doesn't seem to work.</param>
        protected void UpdateListBox(string Name, ref MyPairList Options, int Selected = -1, string SelectedValue = "", int Width = 150, bool Enabled = true)
        {
            divToUpdate.Add(Name + "_div", FormListBox(Name, ref Options, Selected, SelectedValue, Width, Enabled));
        }

        /// <summary>
        /// Return the string required to create a web page list box.
        /// </summary>
        protected string FormListBox(string Name, ref MyPairList Options, int Selected = -1, string SelectedValue = "", int Width = 150, bool Enabled = true)
        {
            UsesjqListBox = true;

            jqListBox lb = new jqListBox(Name, this.PageLink)
            {
                //id = id_prefix + Name,
                style = "width: " + Width + "px;",
                enabled = Enabled,
            };
            lb.items.Clear();
            if (Options != null)
            {
                for (int i = 0; i < Options.Count; i++)
                {
                    if ((Selected == -1) && (SelectedValue == Options[i].ValueStr))
                        Selected = i;
                    lb.items.Add(Options[i].ValueStr);
                }
                if (Selected >= 0)
                    lb.SelectedValue = Options[Selected].ValueStr;
            }

            return lb.Build();
        }

        #endregion ListBox


        #region DropList

        /// <summary>
        /// Build a drop list for a web page.
        /// </summary>
        /// <param name="Name">The name used to create the references for the list box</param>
        /// <param name="Options">Data value pairs used to populate the list box.</param>
        /// <param name="Selected">Index of the item to be selected.</param>
        /// <param name="SelectedValue">Name of the value to be selected.</param>
        /// <returns>The text to insert in the web page to create the drop list.</returns>
        protected string BuildDropList(string Name, ref MyPairList Options, out MyPair selectedPair, int Selected = -1, string SelectedValue = "", bool AddBlankRow = false, int width = 150)
        {
            return "<div id='" + Name + "_div'>" + FormDropDown(Name, ref Options, Selected, out selectedPair,
                SelectedValue: SelectedValue, AddBlankRow: AddBlankRow, width: width) + "</div>";
        }


        protected string BuildDropList(string Name, string[,] Options, out MyPair selectedPair, int Selected = -1, string SelectedValue = "", bool AddBlankRow = false, int width = 150)
        {
            MyPairList _Options = new MyPairList(Options);
            return BuildDropList(Name, ref _Options, out selectedPair, Selected, SelectedValue, AddBlankRow, width);
        }


        /// <summary>
        /// Buld list of values from-to with step
        /// </summary>
        /// <param name="Name">The name used to create the references for the list box</param>
        /// <param name="from">Starting value</param>
        /// <param name="to">End value</param>
        /// <param name="step">Step</param>
        /// <param name="sel">Selected value, should match exactly one of the calculated values</param>
        /// <param name="suff">Suffix to add</param>
        /// <returns></returns>
        public string BuildDropList(string Name, double from, double to, double step, double? sel, string suff = "")
        {
            string[,] pairs = MakePairsArray(from, to, step, suff);
            return BuildDropList(Name, pairs, out MyPair _, SelectedValue: $"{sel}");
        }

        /// <summary>
        /// Helper for BuildDropList above
        /// </summary>
        /// <param name="from">Starting value</param>
        /// <param name="to">End value</param>
        /// <param name="step">Step</param>
        /// <param name="suff">Suffix to add</param>
        /// <returns></returns>
        public string[,] MakePairsArray(double from, double to, double step, string suff = "")
        {
            // Find format precision, i.e for step=0.1 => "0.0" => {0:0.0}{suff}
            string fmt = Regex.Replace($"{step}", @"\d", "0");
            fmt = $"{{0:{fmt}}}{suff}";

            //int n = (int)((to - from + 1) / step);
            int n = 1 + (int)((to - from) / step);
            string[,] pairs = new string[n, 2];
            double v = from;
            for (int i = 0; i <= pairs.GetUpperBound(0); i++)
            {
                pairs[i, 0] = String.Format(fmt, v);
                pairs[i, 1] = $"{v}";
                v += step;
            }
            return pairs;
        }

        /// <summary>
        /// Update a drop list for a web page that was created with a DIV tag.
        /// </summary>
        /// <param name="Name">The name used to create the references for the list box.</param>
        /// <param name="Options">Data value pairs used to populate the list box.</param>
        /// <param name="Selected">Index of the item to be selected.</param>
        /// <param name="SelectedValue">Name of the value to be selected.</param>
        protected void UpdateDropList(string Name, ref MyPairList Options, out MyPair selectedPair, int Selected = -1, string SelectedValue = "")
        {
            divToUpdate.Add(Name + "_div", FormDropDown(Name, ref Options, Selected, out selectedPair, SelectedValue: SelectedValue));
        }

        #endregion DropList

        #region DropDown

        /// <summary>
        /// Return the string required to create a web page drop list
        /// </summary>
        protected string FormDropDown(string Name,
                                       ref MyPairList Options,
                                       int selected,
                                       out MyPair selectedPair,
                                       int width = 150,
                                       bool SubmitForm = true,
                                       bool AddBlankRow = false,
                                       bool AutoPostback = true,
                                       string Tooltip = "",
                                       bool Enabled = true,
                                       string blankText = "",
                                       string SelectedValue = "")
        {
            return FormDropDown(Name, PageLink, ref Options, selected, out selectedPair, width, SubmitForm, AddBlankRow, AutoPostback, Tooltip, Enabled, blankText, SelectedValue);
        }

        /// <summary>
        /// Return the string required to create a web page drop list
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="pageName"></param>
        /// <param name="Options">List of Options</param>
        /// <param name="selected">Slected item index, see also SelectedValue</param>
        /// <param name="selectedPair">returns selected Pair</param>
        /// <param name="width"></param>
        /// <param name="SubmitForm"></param>
        /// <param name="AddBlankRow">add empty row on top</param>
        /// <param name="AutoPostback"></param>
        /// <param name="Tooltip"></param>
        /// <param name="Enabled"></param>
        /// <param name="blankText">Text for AddBlankRow above</param>
        /// <param name="SelectedValue"></param>
        /// <returns></returns>
        public static string FormDropDown(string Name,
                                       string pageName,
                                       ref MyPairList Options,
                                       int selected,
                                       out MyPair selectedPair,
                                       int width = 150,
                                       bool SubmitForm = true,
                                       bool AddBlankRow = false,
                                       bool AutoPostback = true,
                                       string Tooltip = "",
                                       bool Enabled = true,
                                       string blankText = "",
                                       string SelectedValue = "")
        {
            jqDropList dd = new jqDropList(/*id_prefix + */Name, pageName, SubmitForm)
            {
                selectedItemIndex = -1,
                //id = id_prefix + Name,
                autoPostBack = AutoPostback,
                toolTip = Tooltip,
                style = "width: " + width + "px;",
                enabled = Enabled
            };

            selectedPair = new MyPair();

            //Add a blank area to the top of the list
            if (AddBlankRow)
                dd.AddItem(blankText, "", false);

            if (Options != null)
            {
                for (int i = 0; i < Options.Count; i++)
                {
                    bool sel = (i == selected) || (Options[i].ValueStr == SelectedValue);
                    if (sel)
                        selectedPair = Options[i];
                    dd.AddItem(Options[i].Name, Options[i].ValueStr, sel);
                }
            }

            if (dd.selectedItemIndex == -1 && AddBlankRow)
                dd.selectedItemIndex = 0;

            return dd.Build();
        }

        #endregion DropDown

        #region TimeSpanPicker/TimePicker

        protected string FormTimeSpanPicker(string Name,
                                             string Label = "",
                                             bool showSeconds = true,
                                             bool showDays = false,
                                             TimeSpan defaultTimeSpan = default(TimeSpan),
                                             bool SubmitForm = true,
                                             string tooltip = null)
        {
            UsesjqTimeSpanPicker = true;

            jqTimeSpanPicker ts = new jqTimeSpanPicker(Name, Label, this.PageLink, SubmitForm)
            {
                toolTip = tooltip ?? $"{Name} {Label} {defaultTimeSpan}",
                showSeconds = showSeconds,
                showDays = showDays,
                defaultTimeSpan = defaultTimeSpan
            };
            return ts.Build();
        }

        protected string FormTimePicker(string Name,
                                        string Label = "",
                                        bool showSeconds = false,
                                        TimeSpan defaultTimeSpan = default(TimeSpan),
                                        bool SubmitForm = true,
                                        string tooltip = null)
        {
            UsesJqTimePicker = true;

            jqTimePicker tp = new jqTimePicker(Name, Label, this.PageLink, SubmitForm)
            {
                toolTip = tooltip ?? $"{Name} {Label} {defaultTimeSpan}",
                showSeconds = showSeconds,
                minutesSeconds = false,
                editable = true,
                size = 4,
                defaultValue = defaultTimeSpan.ToString("h\\:mm") // TEMP - depends on ampm and showSeconds
            };
            return tp.Build();
        }

        #endregion TimeSpanPicker

        #region FileUploader/LocalFileSelector

        protected string FormFileUploader(string Name,
                                        string Label = "",
                                        string extensions = "",
                                        string acceptFiles = null,
                                        bool SubmitForm = true,
                                        string tooltip = null,
                                        int width = 0,
                                        string pageName = null)
        {
            UsesjqFileUploader = true;

            if (pageName == null)
                pageName = this.PageLink;

            jqFileUploader fu = new jqFileUploader(Name, pageName)
            {
                toolTip = tooltip ?? $"{Name} {Label}",
                label = Label,
                acceptFiles = acceptFiles,
                width = width
            };

            foreach (string ext in extensions.Split(','))
                fu.AddExtension($"*.{ext}");

            fu.values.Add("uploadfile", "true");

            return fu.Build();
        }


        protected string FormLocalFileSelector(string Name,
                                        string Label,
                                        string Path,
                                        string extensions = "",
                                        string acceptFiles = null,
                                        bool SubmitForm = true,
                                        string tooltip = null)
        {
            UsesjqFileUploader = true;

            jqLocalFileSelector fu = new jqLocalFileSelector(Name, this.PageLink, SubmitForm)
            {
                toolTip = tooltip ?? $"{Name} {Label}",
                dialogCaption = Label,
                path = Path
            };

            foreach (string ext in extensions.Split(','))
                fu.AddExtension($"*.{ext}");

            return fu.Build();
        }


        #endregion FileUploader/LocalFileSelector
    }
}