﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Scheduler.clsJQuery;

namespace HSPI_AKTemplate
{
    public partial class PageBuilder
    {
        /// <summary>
        /// Generate jqRadioButton for given Enum type
        /// </summary>
        /// <param name="en">The Enum value</param>
        /// <param name="name">Control ID</param>
        /// <param name="enabled"></param>
        /// <param name="names">If passed, use names from this array, instaed of Enum names</param>
        /// <param name="style">Display style, i.e. Button or Radio</param>
        /// <param name="supported">Limit List of Enum values which should be displayed</param>
        /// <returns></returns>
        public string RadioButtonEnum(Enum en, string name, bool enabled = true, string[] names = null, string style = null, object[] supported = null)
        {
            if (!name.StartsWith("radio_"))
                name = $"radio_{name}";

            jqRadioButton rb = new jqRadioButton(name, this.PageLink, true)
            {
                buttonset = false,
                enabled = enabled,
                style = style
            };

            foreach (object val in Enum.GetValues(en.GetType()))
            {
                if (supported != null /*&& supported.Length > 0*/ && !supported.Contains(val))
                    continue;

                int i = (int)val;

                string s = Enum.GetName(en.GetType(), i);

                if (s == en.ToString())
                    rb.@checked = $"{i}";

                if (names != null)
                    s = names[i];

                if (s != "-" && s != "unknown")
                    rb.values.Add($"{s}", $"{i}");
            }

            return rb.Build();
        }

        /// <summary>
        /// Process radio button control selection
        /// For radio buttons the selected value is appended to control id (after underscore)
        /// i.e.
        /// id = radio_grp_2_logic_1
        /// </summary>
        /// <param name="ctrlID"></param>
        /// <param name="value"></param>
        /// <param name="parts"></param>
        /// <returns></returns>
        public static bool ProcessRadioBtn(ref string ctrlID, ref string value, NameValueCollection parts)
        {
            // i.e. "radio_ctrlType_1" - where last _1 means selected value
            GroupCollection match = Regex.Match(ctrlID, @"radio_(.+)_(\d)").Groups;
            if (match.Count == 3)
            {
                try
                {
                    ctrlID = match[1].Value;
                    value = parts[$"radio_{ctrlID}"];
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Can't process {ctrlID}: {ex.Message}");
                }
            }
            return false;
        }


        /// <summary>
        /// Create string "name='value'" if val>=0
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="def">Default string if val==-1</param>
        /// <returns></returns>
        public static string fmtHTMLPair(string name, int value, string def = "")
        {
            return fmtHTMLPair(name, (value >= 0 ? value.ToString() : null), def);
        }
        public static string fmtHTMLPair(string name, string value, string def = "")
        {
            return (!String.IsNullOrEmpty(value) ? $"{name}='{value}'" : def);
        }


        /// <summary>
        /// Create HTML for image
        /// </summary>
        /// <param name="file"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="tolink"></param>
        /// <param name="aliign">String for align attribute, i.e. "absmiddle"</param>
        /// <param name="addCnt">Add "?cnt=" to avoid caching </param>
        /// <returns></returns>
        public static string HTML_Img(string file, int width = -1, int height = -1, bool tolink = false, string aliign = "absmiddle", bool addCnt = false)
        {
            string scnt = addCnt ? $"?cnt={++cnt}" : "";

            string html = $"<img src='{file}{scnt}' " +
                          $" {fmtHTMLPair("align", width)}" +
                          $" {fmtHTMLPair("width", width)}" +
                          $" {fmtHTMLPair("height", height)}/>";

            if (tolink)
                html = $"<a href='{file}' target='_new'>{html}</a>";

            return html;
        }

        static int cnt = 0;


        /// <summary>
        /// Return "check" image HTML if val==true
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string BoolToImg(bool val)
        {
            return val ? HTML_Img("images/HomeSeer/ui/active.png", 15) : "";
        }


        /// <summary>
        /// Create a Menu button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="label"></param>
        /// <param name="url">Can be a link, not button</param>
        /// <returns></returns>
        public string MenuButton(string id, string label, string CurrentTab = null, string url = null)
        {
            bool selected = (CurrentTab != null) ? (id == CurrentTab) : false;
            string style = "font-size: 100%; padding: 3px 0; width: 120px !important;" + (selected ? " border: 1px solid #5799DF;" : "");
            return FormButton(Name: id,
                               label: label,
                               url: url,
                               Style: style,
                               className: selected ? "functionrowbuttonselected" : "functionrowbutton",
                               width: 120
                               );
        }

        /// <summary>
        /// Generate HTML for Delete button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string DeleteBtn(string id, string ToolTip = "", string value = "", bool small = false)
        {
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/Delete.png", width: small ? 15 : 0);
        }

        /// <summary>
        /// Generate HTML for Add button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string AddBtn(string id, string ToolTip = "", string value = "", bool small = false)
        {
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/Add.png", width: small ? 15 : 0);
        }

        /// <summary>
        /// Generate HTML for Add button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string CopyBtn(string id, string ToolTip = "", string value = "", bool small = false)
        {
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/duplicate.png", width: small ? 15 : 0);
        }

        /// <summary>
        /// Generate HTML for Edit button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string EditBtn(string id, string ToolTip = "", string value = "", bool small = false)
        {
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/Edit.gif", width: small ? 15 : 0);
        }

        /// <summary>
        /// Generate HTML for Save button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string SaveBtn(string id, string ToolTip = "", string value = "", bool small = false)
        {
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/Save.png", width: small ? 15 : 0);
        }

        /// <summary>
        /// Generate HTML for Edit button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public string NoteBtn(string id, string ToolTip = "", string value = "checked", bool small = false)
        {
            // label: "checked" - immitate checkbox click
            return FormButton(id, label: value, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/note.gif", width: small ? 15 : 0);
        }

        public static string RunBtnPrefix = "set_";
        // To generate unique btn id on the page (ignored, but necessary)
        private static int runBtnId = 0;

        /// <summary>
        /// Generate HTML for Run button
        /// </summary>
        /// <param name="devID"></param>
        /// <param name="value"></param>
        /// <param name="ToolTip"></param>
        /// <param name="small"></param>
        /// <returns></returns>
        public string RunBtn(int devID, object value, string Label = "", bool small = false)
        {
            string ToolTip = $"Test '{Label} ({value})' Ref. {devID}";
            string id = $"{RunBtnPrefix}{++runBtnId}_{devID}";
            string valstr = $"{value}";
            return FormButton(id, valstr, ToolTip: ToolTip, ImagePathNormal: "images/HomeSeer/ui/run-event.png", width: small ? 15 : 0);
        }

        /// <summary>
        /// Extract devID from string created by RunBtn()
        /// </summary>
        /// <param name="devIdStr"></param>
        /// <returns></returns>
        public static int ExtractCtrlIdRunBtn(string devIdStr)
        {
            devIdStr = devIdStr.Replace(RunBtnPrefix, "");
            string[] s = devIdStr.Split('_');
            int.TryParse(s[1], out int devID);
            return devID;
        }


        /// <summary>
        /// Generate HTML for Edit button
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title">Tooltip</param>
        /// <returns></returns>
        public static string MyToolTip(string text, bool addspace = true, string url = null, bool error = false, bool small = true, string image = null)
        {
            jqButton b = new jqButton($"tt{++toolTipCnt}", text, null, false)
            {
                url = url,
                enabled = (url != null),
                hyperlink = false,
                urlNewWindow = true,
                imagePathNormal = image != null ? image : error ? "images/HomeSeer/ui/Caution-red.png" : "images/HomeSeer/ui/help-button-sm.png",
                toolTip = text,
                width = small ? (error ? 15 : 10) : 20
            };

            string space = addspace ? "&nbsp;&nbsp;" : "";
            return space + b.Build().Replace("</button>\r\n", "</button>").Trim();
        }

        private static int toolTipCnt = 0;



        /// <summary>
        /// Convert a list string to a table, for inserting inside another table cell
        /// </summary>
        /// <param name="lst">List of values in a string "a,b,c"</param>
        /// <param name="sep"></param>
        /// <returns></returns>
        public static string ListToTable(string lst, char sep = ',')
        {
            return ListToTable(lst.Split(sep));
        }

        public static string ListToTable(string[] strings, char sep = ',')
        {
            string attrs = "max_width='300px' style='margin: 5px; display: inline-table'";
            var table = new TableBuilder(null, ncols: strings.Length, attrs: attrs);

            foreach (string s in strings)
            {
                table.AddCell(s.Trim());
            }

            return table.Build(addBR: false);
        }
    }
}