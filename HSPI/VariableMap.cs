﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSPI_AKTemplate
{
    /// <summary>
    /// Structure for mapping control ID to variable name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Variable<T>
    {
        public string Name;
        public string CtrlName;
        public object parent;
        public Type type;
        public bool table_slider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name">Variable name</param>
        /// <param name="CtrlName">Control name</param>
        /// <param name="parent">Object containing the variable/property</param>
        /// <param name="type"></param>
        public Variable(string Name, string CtrlName, object parent, Type type)
        {
            this.Name = Name;
            this.CtrlName = CtrlName;
            this.parent = parent;
            this.type = type;
            table_slider = false;
        }
    }



    /// <summary>
    /// Dictionary of Structures for mapping control ID to variable name
    /// </summary>
    public class VariableMap : Dictionary<string, object>
    {
        /// <summary>
        /// Function for making the dictionary key unique, as the dictionary is static
        /// </summary>
        Func<string, string> MakeKey;

        /// <summary>
        /// Default just return the key itself
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string _MakeKey(string key)
        {
            return key.ToLower();
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public VariableMap()
        {
            // Set to default empty function
            MakeKey = _MakeKey;
        }


        /// <summary>
        /// Add the mapping control id => variable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name">For simplicity use same name for the control id and variable name</param>
        /// <param name="uniqueControlId">From BuildUI, to append to Name</param>
        /// <param name="parent">Object containing the variable/property</param>
        /// <param name="_MakeKey">Function for making the dictionary key unique, as the dictionary is static</param>
        /// <returns></returns>
        public string SetName<T>(string Name, string uniqueControlId, object parent, Func<string, string> _MakeKey = null)
        {
            if (_MakeKey != null)
                MakeKey = _MakeKey;

            string CtrlName = $"{Name.ToLower()}{uniqueControlId}";
            string UniqueCtrlName = MakeKey(CtrlName);

            SetName<T>(Name, CtrlName, UniqueCtrlName, parent);

            return CtrlName;
        }


        /// <summary>
        /// Add the mapping control id => variable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name">Name of variable</param>
        /// <param name="CtrlName">Name of control</param>
        /// <param name="UniqueCtrlName"></param>
        /// <param name="parent">Object containing the variable/property</param>
        public Variable<T> SetName<T>(string Name, string CtrlName, string UniqueCtrlName, object parent)
        {
            this[UniqueCtrlName] = new Variable<T>(Name, CtrlName, parent, typeof(T));
            return (Variable<T>)this[UniqueCtrlName];
        }


        /// <summary>
        /// Find the control id => variable name mapping and set the mapped variable
        /// </summary>
        /// <param name="CtrlName">Control name</param>
        /// <param name="StrVal">value to set (string)</param>
        /// <param name="parent1">Reference to parent stored in the map seems to be different</param>
        /// <param name="_MakeKey">Function for making the dictionary key unique, as the dictionary is static</param>
        /// <returns></returns>
        public bool CheckVariable(string CtrlName, string StrVal, object parent1, Func<string, string> _MakeKey = null)
        {
            if (_MakeKey != null)
                MakeKey = _MakeKey;

            string UniqueCtrlName = MakeKey(CtrlName);

            if (!this.ContainsKey(UniqueCtrlName))
                return false;

            dynamic varmap = this[UniqueCtrlName];
            dynamic value = null;

            if (varmap.table_slider)
            {
                // CtrlName is actually table name
                string slide = TableBuilder.SliderName(varmap.CtrlName);
                TableBuilder.TrySliderSet(slide, StrVal, out string tableID, out bool? state);
                //Debug.Assert(varmap.CtrlName == tableID);
                value = state;
            }
            else
            {
                value = Utils.ConvertValue(StrVal, varmap.type, enumAsInt: false);
            }

            dynamic set = Utils.getInstancePropertyOrMember(varmap.parent, varmap.Name, value);

            if(set == value)
                return true;

            Utils.setInstancePropertyOrMember(varmap.parent, varmap.Name, value);

            // Reference to parent stored in the map seems to be different
            if(parent1 != null)
                Utils.setInstancePropertyOrMember(parent1, varmap.Name, value);

            return false;
        }


        /// <summary>
        /// Find the control id => variable name mapping and set the mapped variable
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="parent1">Reference to parent stored in the map seems to be different</param>
        /// <param name="_MakeKey"></param>
        /// <returns></returns>
        public bool CheckVariables(NameValueCollection postData, object parent1, Func<string, string> _MakeKey = null)
        {
            if (_MakeKey != null)
                MakeKey = _MakeKey;

            bool rv = false;

            foreach (string key in postData.AllKeys)
            {
                string value = postData[key];

                // If some variable was changed - return true
                if (CheckVariable(key, value, parent1, _MakeKey))
                    rv = true;
            }
            return rv;
        }
    }
}
