﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;


using Scheduler;
using HomeSeerAPI;
using static HomeSeerAPI.VSVGPairs;
using static HomeSeerAPI.PlugExtraData;
using static HomeSeerAPI.DeviceTypeInfo_m.DeviceTypeInfo;
using static HomeSeerAPI.Enums;

namespace HSPI_AKTemplate
{
    using HSDevice = Scheduler.Classes.DeviceClass;

    /// <summary>
    /// Create minimal device if used only for reading device config(PED)
    /// 
    /// NOTE: DeviceInfo implements both IObservable and IObserver
    /// so each device can subscribe to another device - which is cool
    /// But it complicates things as device is now able to subscribe to itself
    /// And need to check using "Unsubscriber : IDisposable" - make sure it doesn't Dispose wrong device
    ///
    /// </summary>
    public partial class DeviceBase
    {
        public static string ADDR_PREFIX = "AK";

        // Underlying (wrapped) HS devise
        public HSDevice deviceHS { get; protected set; }

        // Only for getting StateDevice from UsedDevices, instead of creating a new one
        protected ControllerBase controller = null;


        // Set to false, in case of error - set to true
        protected bool debug = false; // TEMP - settings?


        #region Construction


        /// <summary>
        /// Constructor
        /// Pass either devID or deviceHS
        /// </summary>
        /// <param name="controller">Controller</param>
        /// <param name="devID">HSDevice</param>
        /// <param name="deviceHS">HSDevice</param>
        public DeviceBase(ControllerBase controller, int devID = 0)
        {
            FullUpdate = false;
            this.controller = controller;
            this.RefId = devID;

            //if (deviceHS==null)
            {
                deviceHS = controller.GetHSDeviceByRef(devID);
                if (deviceHS == null)
                {
                    // Deleted device. This also sets "Attension" field
                    if (devID > 0)
                        LogErr("Device doesn't exist in the system");
                    return;
                }
            }

            this.deviceHS = deviceHS;
            this.RefId = deviceHS.get_Ref(null);
        }

        /// <summary>
        /// update device info
        /// </summary>
        /// <param name="force">COMMENT</param>
        public void update_info(bool force = true, bool reset_err = false)
        {
            // Store current setting and temporary set to true if 'force'
            bool fu = this.FullUpdate;
            FullUpdate = force;

            Name = null;
            _ped = null;

            // Set device Type to "Virtual", only if it's not set yet and only for my plugin
            CheckType(deflt: "Virtual");

            // restore prvious setting
            FullUpdate = fu;

            if(reset_err)
            {
                Attention = null; // Remove "Attention"
                Error = "";       // Reset Error
            }
        }

        public override string ToString()
        {
            return $"(Ref: {RefId}) {Name} ({DeviceString})";
        }

        public virtual bool Create()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a HomeSeer device if doesn't exist
        /// </summary>
        /// <param name="addr">Device address (used to find device if exists)</param>
        /// <param name="force"></param>
        /// <param name="type">String device type</param>
        /// <param name="Device_API"></param>
        /// <param name="Device_Type"></param>
        /// <param name="Device_SubType"></param>
        /// <param name="Device_SubType_Description"></param>
        /// <param name="dev_name">Optional device name to assign when creating device</param>
        /// <returns></returns>
        public bool Create( string addr,
                            bool force = false,
                            string type = "Virtual",
                            eDeviceAPI Device_API = eDeviceAPI.Plug_In,
                            int Device_Type = (int)eDeviceAPI.Plug_In,
                            int Device_SubType = 0,
                            string Device_SubType_Description = "",
                            string dev_name = null
                           )
        {
            if (dev_name == null)
                dev_name = addr;

            addr = ADDR_PREFIX + addr;

            deviceHS = controller.GetHSDeviceByAddress(addr, out bool created, create: true, name: dev_name);

            update_info(reset_err: true); // Reset _ped and Error

            RefId = deviceHS.get_Ref(null);

            if (created)
            {
                DeviceTypeInfo_m.DeviceTypeInfo DT = new DeviceTypeInfo_m.DeviceTypeInfo()
                {
                    Device_API = Device_API,
                    Device_Type = Device_Type,
                    Device_SubType = Device_SubType,
                    Device_SubType_Description = Device_SubType_Description
                };
                deviceHS.set_DeviceType_Set(hs, DT);

                deviceHS.set_Status_Support(hs, false);//Set to True if the devices can be polled,  false if not
                SetMisc(Enums.dvMISC.SHOW_VALUES | Enums.dvMISC.NO_LOG);
                SetMisc(Enums.dvMISC.AUTO_VOICE_COMMAND, false);

                Interface = controller.plugin.Name;
                InterfaceInstance = ""; // TEMP - TODO: Implement InterfaceInstance
                Type = type;
                Address = addr;
            }

            controller.UsedDevices[RefId] = this;

            return created;
        }


        // Just to avoid deleting/logging twice (because of notification from HS
        bool deleted = false;

        /// <summary>
        /// This function removes a device from HomeSeer.
        /// Use this function with caution!
        /// </summary>
        /// <returns>Indicates the success or failure of the operation</returns>
        public virtual bool DeleteDevice()
        {
            if (deleted)
                return false;

            deleted = true;
            LogErr($"Deleted device '{FullName}' (Ref. {RefId}, Addr. {Address})");
            return hs.DeleteDevice(this.RefId);
        }

        #endregion Construction


        #region Relationship

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(DeviceBase parent)
        {
            parent.AddAssociatedDevice(this);
            parent.SetRelationship(eRelationship.Parent_Root);
            AddAssociatedDevice(parent);
            SetRelationship(eRelationship.Child);
        }

        /// <summary>
        /// Adds the associated device.
        /// </summary>
        /// <param name="dvRef">The dv reference.</param>
        public void AddAssociatedDevice(int dvRef) => deviceHS.AssociatedDevice_Add(hs, dvRef);

        /// <summary>
        /// Adds the associated device.
        /// </summary>
        /// <param name="device">The device.</param>
        public void AddAssociatedDevice(DeviceBase device) => AddAssociatedDevice(device.RefId);

        /// <summary>
        /// Sets the relationship.
        /// </summary>
        /// <param name="value">The value.</param>
        public void SetRelationship(eRelationship value) => deviceHS.set_Relationship(hs, value);


        #endregion Relationship


        #region Properties


        /// <summary>
        /// If device is created from Controller.UpdateConfiguration - 
        /// we are iterating ALL devices - it's unefficient to go to HS for each set/get call
        /// Particulary slow is get_Interface call (don't know why)
        /// And it's unnecessary - UpdateConfiguration calls UpdateDeviceList() anyway
        /// So set IHSApplication for most calls to null.
        /// But for plugin.ConfigDevice() it's safer to go to HS for all set/get calls
        /// So set IHSApplication for most calls to Utils.Hs
        /// </summary>
        public bool FullUpdate { get; set; }


        /// <summary>
        /// See comment for FullUpdate
        /// </summary>
        protected IHSApplication _hs_full_update
        {
            get { return FullUpdate ? hs : null; }
        }

        /// <summary>
        /// Make static here - instead of Utils?
        /// </summary>
        protected IHSApplication hs
        {
            get
            {
                // Use controller.HS, not controller.plugin.HS
                // As it's reset to the new HS when connection is restored
                // But only after UpdateConfiguration is completed!
                return controller.HS;
            }
        }


        public int RefId { get; protected set; }


        /// <summary>
        /// Set Devive Name, but doesn't change in HS!
        /// </summary>
        public string Name
        {
            get
            {

                if (_name == null)
                    try
                    {
                        _name = deviceHS.get_Name(_hs_full_update);
                    }
                    catch(Exception)
                    { }
                 return _name;
            }

            protected set { _name = value; }
        }

        string _name = null;

        public string HS_Name
        {
            set
            {
                Name = value;
                if (deviceHS != null)
                    deviceHS.set_Name(hs, Name);
            }
        }

        public string FullName
        {
            get
            {
                return $"[{Location}] [{Location2}] {Name}";
            }
        }

        public string Address
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Address(_hs_full_update);
            }
            set
            {
                if (value != Address)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_Address(hs, value);
                }
            }
        }

        public string Type
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Device_Type_String(_hs_full_update);
            }
            set
            {
                if (value != Type)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_Device_Type_String(hs, value);
                }
            }
        }

        public string LastChange
        {
            get
            {
                if (deviceHS == null) return null;
                DateTime ts = this.deviceHS.get_Last_Change(hs);
                return Utils.ToString(ts);
            }

            //protected set
            //{
            //    this.deviceHS.set_Last_Change(hs, value);
            //}
        }

        public string Location
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Location(_hs_full_update);
            }
            set
            {
                if (value != Location)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_Location(hs, value);
                }
            }
        }

        public string Location2
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Location2(_hs_full_update);
            }
            set
            {
                if (value != Location2)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_Location2(hs, value);
                }
            }
        }

        public string Interface
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Interface(_hs_full_update);
            }
            protected set
            {
                if (value != Interface)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_Interface(hs, value);
                }
            }
        }

        public string InterfaceInstance
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_InterfaceInstance(_hs_full_update);
            }
            protected set
            {
                if (value != InterfaceInstance)
                {
                    FullUpdate = true; // once write to HS - need to read from HS too
                    deviceHS.set_InterfaceInstance(hs, value);
                }
            }
        }

        /// <summary>
        /// Set device status image to exclamation in case of error
        /// </summary>
        public string Attention
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Attention(_hs_full_update);
            }
            set
            {
                try
                {
                    if (deviceHS != null)
                        deviceHS.set_Attention(hs, value);
                }
                catch(Exception)
                {

                }
            }
        }


        /// <summary>
        /// Set device image icon
        /// </summary>
        public string DeviceImage
        {
            get
            {
                if (deviceHS == null) return null;
                return deviceHS.get_Image(_hs_full_update);
            }
            set
            {
                if (deviceHS != null)
                {
                    deviceHS.set_Image(hs, value);
                    deviceHS.set_ImageLarge(hs, value);
                }
            }
        }

        /// <summary>
        /// Set/Clear MISC bits
        /// </summary>
        /// <param name="flag">List of flags</param>
        /// <param name="set">set/clear</param>
        public void SetMisc(dvMISC flag, bool set = true)
        {
            if (deviceHS != null)
            {
                if (set)
                    deviceHS.MISC_Set(hs, flag);
                else
                    deviceHS.MISC_Clear(hs, flag);
            }
        }


        public bool power_fail_recovery
        {
            get
            {
                if (deviceHS == null) return false;
                return deviceHS.MISC_Check(hs, Enums.dvMISC.INCLUDE_POWERFAIL);
            }
            set
            {
                SetMisc(Enums.dvMISC.INCLUDE_POWERFAIL, value);
            }
        }

        public bool Hidden
        {
            get
            {
                if (deviceHS == null) return false;
                return deviceHS.MISC_Check(hs, dvMISC.HIDDEN);
            }
            set
            {
                SetMisc(dvMISC.HIDDEN, value);
            }
        }

        /// <summary>
        /// Get current status image url relative to html/ i.e. /image/imaje.jpg
        /// </summary>
        public string Graphic => hs.DeviceVGP_GetGraphic(RefId, Value);

        string _ScaleText;

        /// <summary>
        /// At the time a device is created, it may not be known whether its scale is meters
        /// or Miles, Fahrenheit or Celcius, or some other set of multiple scales.To help with
        /// those situations, set HasScale to True, use the constant ScaleReplace in your
        /// range prefix or suffix, and then at runtime when the device is being updated to a
        /// new value, set the device ScaleText to your scale(e.g. "degF" or "degC"), set the
        /// value, and then when the status is requested, HomeSeer will replace
        /// ScaleReplace(@S@) with your ScaleText.
        /// </summary>
        public string ScaleText
        {
            set
            {
                if (_ScaleText == value || hs == null || deviceHS == null)
                    return;

                _ScaleText = value;
                deviceHS.set_ScaleText(hs, _ScaleText);
            }

            get
            {
                if (hs == null || deviceHS == null)
                    return _ScaleText;

                return deviceHS.get_ScaleText(_hs_full_update);
            }
        }

        /// <summary>
        /// Set/get device string
        /// </summary>
        public string DeviceString
        {
            set
            {
                if(hs==null || _deviceString == value)
                {
                    //Console.WriteLine($"DeviceString {RefId}: hs is null");
                    return;
                }

                _deviceString = value;

                if (DeviceStringToUrl && !String.IsNullOrEmpty(_deviceString) && _deviceString.Contains("http"))
                {
                    // Convert to link
                    _deviceStringUrl = $"<a href='{_deviceString}' target='_new'>{_deviceString}</a>";
                    hs.SetDeviceString(RefId, _deviceStringUrl, reset: true);
                }
                else
                {
                    hs.SetDeviceString(RefId, _deviceString, reset: true);
                }
            }

            get
            {
                string str = hs.DeviceString(RefId);
                // If hs.DeviceString failed - see if we have previous value
                if (String.IsNullOrEmpty(str))
                {
                    str = _deviceString;
                }
                // If hs.DeviceString failed - try to use CAPI
                if (String.IsNullOrEmpty(str))
                {
                    CAPI.ICAPIStatus st = hs.CAPIGetStatus(RefId);
                    str = st.Status;
                }
                // If CAPI failed too - just return 'Value'
                if (String.IsNullOrEmpty(str))
                    str = Value.ToString();
                return str;
            }
        }

        string _deviceString;
        string _deviceStringUrl;
        protected bool DeviceStringToUrl = true;

        /// <summary>
        /// For setting Value - use CAPIControlHandler or SetDeviceValueByRef
        /// </summary>
        public enum ControlType
        {
            CAPI = 0,     // Use CAPIControlHandler - set to 0 to make it default value
            Both = 1,     // Use both
            SetByRef = 2, // Use SetDeviceValueByRef
        }

        /// <summary>
        /// Set device Value
        /// Note: If changing Value by plugin - should use CAPIControl
        ///       If called from SetIOMulti - can't use CAPIControl, must use SetDeviceValueByRef
        ///       So controller set this to true, and here we reset it back to false
        /// Note: if for some devices (probably othe plugin bug) CAPIControl updates device,
        ///       but not HS UI - can use both SetDeviceValueByRef and CAPIControl 
        ///       (probably never happens and can be removed in the future)
        /// </summary>
        public bool forceSetDeviceValueByRef = false;

        public double Value
        {
            get
            {
                if (deviceHS == null) return 0;
                ValueCached = this.deviceHS.get_devValue(hs);
                return ValueCached;
            }

            set
            {
                try
                {
                    ControlType type = ControlType.CAPI;

                    // If called from SetIOMulti - can't use CAPIControl, must use SetDeviceValueByRef
                    if (forceSetDeviceValueByRef)
                    {
                        type = ControlType.SetByRef;
                        forceSetDeviceValueByRef = false;
                        if (ValueCached != value)
                        {
                            // Set DeviceString to null (to use default state string) only if state changes
                            _deviceString = null;
                            hs.SetDeviceString(RefId, _deviceString, reset: true); // Can't use DeviceString = null;
                        }
                    }
                    //else
                    {
                        if (ValueCached == value)
                            return;
                    }

                    if (type == ControlType.SetByRef)
                    {
                        hs.SetDeviceValueByRef(this.RefId, value, trigger: true);
                    }

                    if (type == ControlType.CAPI)
                    {
                        CAPI.CAPIControl ctrl = this.statusPairsDict.GetCAPIControl(value);
                        CAPI.CAPIControlResponse ret = hs.CAPIControlHandler(ctrl);
                    }

                    //if (ValueCached != value)
                    //    LogDeb($"SetValue: '{Name}' = '{value}' ({type})");

                    ValueCached = value;
                }
                catch (Exception ex)
                {
                    // Store the state value in case connection is lost
                    val_saved = value;
                    LogErr($"Error setting device value : '{value}'. {ex.Message}");
                }
            }
        }

        /// <summary>
        /// Called from SetIOMulti
        /// </summary>
        /// <param name="value"></param>
        public virtual void SetValue(double value)
        {
            // Called from SetIOMulti - must use SetDeviceValueByRef, not CAPIControl
            forceSetDeviceValueByRef = true; // Device will reset it back
            Value = value;
        }

        /// <summary>
        /// Store the state value in case connection is lost
        /// And restore after
        /// </summary>
        protected double? val_saved = null;

        /// <summary>
        /// For performance keep device state in ValueCached
        /// </summary>
        public double ValueCached
        {
            get => _valueCached;
            set
            {
                // Keep prev. value (only for log)
                ValuePrev = _valueCached;
                _valueCached = value;
            }
        }
        protected double _valueCached = -1;

        /// <summary>
        /// Keep prev. value (only for log)
        /// </summary>
        public double ValuePrev { set; get; }

        /// <summary>
        /// If something wrong happens keep the error string
        /// </summary>
        string _error;
        public string Error
        {
            get => _error;

            set
            {
                _error = value;
                Attention = _error;
            }
        }


        public bool log_enable
        {
            get
            {
                return GetPED<bool>(Utils.FuncName());
            }
            set
            {
                SavePED(Utils.FuncName(), value);
            }
        }



        #endregion Properties


        #region PED Methods

        protected clsPlugExtraData _ped = null;

        public clsPlugExtraData ped
        {
            set
            {
                _ped = value;
                if (this.deviceHS != null)
                {
                    this.deviceHS.set_PlugExtraData_Set(null, _ped);
                    this.deviceHS.set_PlugExtraData_Set(hs, _ped);
                }
            }
            get
            {
                if (_ped==null && deviceHS != null)
                    _ped = this.deviceHS.get_PlugExtraData_Get(hs);
                if (_ped == null)
                    _ped = new clsPlugExtraData();
                return _ped;
            }
        }


        /// <summary>
        /// Save PED to HS
        /// </summary>
        public void UpdatePED()
        {
            if (this.deviceHS != null)
                this.deviceHS.set_PlugExtraData_Set(hs, this.ped);
            hs.SaveEventsDevices();
        }


        public static readonly string ped_name_prefix = "ak_";

        /// <summary>
        /// Save named PED
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pedName">PED Name</param>
        /// <param name="Value"></param>
        /// <returns>true if value changed</returns>
        public bool SavePED<T>(string pedName, T Value)
        {
            bool force = false;

            pedName = CheckPedName(pedName);

            //Console.WriteLine($"SavePED: '{pedName}' = '{Value}'"); // TEMP

            T val_saved = GetPED<T>(pedName, out bool notset);

            // Force save PED if it doesn't exist
            if (notset)
                force = true;

            // Value not changed - return false
            if (!force && 0 == Comparer<T>.Default.Compare(Value, val_saved))
                return false;

            Utils.PedAdd(ref _ped, pedName, Value);

            // Force update PED (set_PlugExtraData_Set)
            UpdatePED();

            return true;
        }

        /// <summary>
        /// Get named PED
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pedName">PED Name</param>
        /// <param name="notset">Returns True if value doesn't exist</param>
        /// <param name="deflt">Default value in case it's not set</param>
        /// <returns></returns>
        public T GetPED<T>(string pedName, out bool notset, T deflt = default(T))
        {
            return Utils.GetPED<T>(CheckPedName(pedName), this.ped, out notset, deflt);
        }

        /// <summary>
        /// Same as above, just ignoring "notset"
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pedName"></param>
        /// <param name="deflt"></param>
        /// <returns></returns>
        public T GetPED<T>(string pedName, T deflt = default(T))
        {
            return GetPED<T>(pedName, out bool notset, deflt);
        }

        /// <summary>
        ///  Get named PED (nullable)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pedName">PED Name</param>
        /// <returns></returns>
        public T GetPEDnull<T>(string pedName)
                    where T : class
        {
            return Utils.GetPEDnull<T>(CheckPedName(pedName), this.ped);
        }

        /// <summary>
        /// Remove named PED
        /// </summary>
        /// <param name="name"></param>
        public bool DeletePED(string pedName)
        {
            bool ret = ped.RemoveNamed(CheckPedName(pedName));
            UpdatePED();
            return ret;
        }

        public static string CheckPedName(string name)
        {
            if (!name.StartsWith(ped_name_prefix))
                name = ped_name_prefix + name;
            return name;
        }

        public Dictionary<string, object> GetAllPEDs(string part = null)
        {
            return Utils.GetAllPEDs(this.ped, part);
        }

        #endregion PED Methods


        #region VSPair

        public void AddVSPairs(double Value,
                              string Control,
                              string Status,
                              ePairControlUse ControlUse,
                              string Graphic = null,
                              int Row = 0,
                              int Column = 0
                              )
        {
            AddVSPair(Value, Control, ControlUse, Graphic, ePairStatusControl.Control, Row: Row, Column: Column);
            AddVSPair(Value, Status, ControlUse, Graphic, ePairStatusControl.Status);
        }

        public VSPair AddVSPair(double Value,
                                string Status,
                                ePairControlUse ControlUse,
                                string Graphic = null,
                                ePairStatusControl Status_Control = ePairStatusControl.Both,
                                int Row = 0,
                                int Column = 0
                                )
        {
            var svPair = new VSPair(Status_Control)
            {
                PairType = VSVGPairType.SingleValue,
                Value = Value,
                Status = Status,
                ControlUse = ControlUse,
                Render = Enums.CAPIControlType.Button,
                IncludeValues = true,
                Render_Location = new CAPIControlLocation
                {
                    Row = Row,
                    Column = Column,
                    ColumnSpan = 1
                }
            };

            bool ret = hs.DeviceVSP_AddPair(RefId, svPair);

            if (Graphic != null)
            {
                ret = SetPairGraphic(Value, Graphic);
            }
            return svPair;
        }


        public VSPair AddVSRangePair(double Start,
                                     double End,
                                     string Prefix = "",
                                     string Suffix = "",
                                     int RangeStatusDecimals = 0,
                                     ePairControlUse ControlUse = ePairControlUse.Not_Specified,
                                     CAPIControlType ControlType = CAPIControlType.ValuesRange,
                                     string Graphic = null,
                                     ePairStatusControl Status_Control = ePairStatusControl.Both,
                                     int Row = 0,
                                     int Column = 0,
                                     int ColumnSpan = 1,
                                     bool HasScale = false
                                    )
        {
            if (HasScale && String.IsNullOrEmpty(Suffix))
                Suffix = VSPair.ScaleReplace; // "@S@";

            var svPair = new VSPair(Status_Control)
            {
                PairType = VSVGPairType.Range,
                RangeStart = Start,
                RangeEnd = End,
                ControlUse = ControlUse,
                Render = ControlType,
                IncludeValues = true,
                RangeStatusPrefix = Prefix,
                RangeStatusSuffix = Suffix,
                RangeStatusDecimals = RangeStatusDecimals,
                HasScale = HasScale,
                Render_Location = new CAPIControlLocation
                {
                    Row = Row,
                    Column = Column,
                    ColumnSpan = ColumnSpan
                }
            };


            bool ret = hs.DeviceVSP_AddPair(RefId, svPair);

            AddVGPair(Start, End, Graphic);

            return svPair;
        }


        public void AddVGPair(double Start,
                              double End,
                              string Graphic)
        {
            if (Graphic != null)
            {
                var vgPair = new VGPair();
                vgPair.PairType = VSVGPairType.Range;
                vgPair.RangeStart = Start;
                vgPair.RangeEnd = End;
                vgPair.Graphic = Graphic;
                bool ret = hs.DeviceVGP_AddPair(RefId, vgPair);
            }
        }

        public VSPair AddVSTemperaturePair(bool C,
                                           double Start = 0,
                                           double End = 35,
                                           string Suffix = null,
                                           int RangeStatusDecimals = 0,
                                           ePairControlUse ControlUse = ePairControlUse.Not_Specified,
                                           CAPIControlType ControlType = CAPIControlType.ValuesRange,
                                           string Graphic = "/images/HomeSeer/status/Thermometer-50.png",
                                           ePairStatusControl Status_Control = ePairStatusControl.Both
                                           )
        {
            return AddVSRangePair(Start,
                                  End,
                                  Suffix: Suffix ?? (C ? " °C" : " °F"),
                                  Graphic: Graphic,
                                  RangeStatusDecimals: RangeStatusDecimals,
                                  ControlUse: ControlUse,
                                  ControlType: ControlType,
                                  Status_Control: Status_Control
                                  );
        }



        public bool SetPairGraphic(double Value, string Graphic)
        {
            VGPair vgPair = hs.DeviceVGP_Get(RefId, Value);
            if(vgPair==null)
                vgPair = new VGPair();
            else
                hs.DeviceVGP_Clear(RefId, Value);

            vgPair.PairType = VSVGPairType.SingleValue;
            vgPair.Set_Value = Value;
            vgPair.Graphic = Graphic;
            return hs.DeviceVGP_AddPair(RefId, vgPair);
        }

        #endregion VSPair


        #region VSPairs


        public StatusPairsDict statusPairsDict
        {
            get
            {
                if (_statusPairsDict == null)
                    _statusPairsDict = new StatusPairsDict(hs, RefId);

                return _statusPairsDict;
            }
        }


        protected StatusPairsDict _statusPairsDict = null;

        /// <summary>
        /// Get all VSPairs, for performance keep them in dict with the key Status/Control/Both
        /// Convert VSVGPairs.VSPair[] to simple PairList
        /// Note: ranges *split* into individual states
        /// </summary>
        /// <param name="StatusControl">ePairStatusControl - Status, Control, or Both</param>
        /// <param name="splitrange">ranges *split* into individual states</param>
        /// <returns></returns>
        public MyPairList vspsList(ePairStatusControl StatusControl, SplitRangeType splitrange)
        {
            return statusPairsDict.vspsList(StatusControl, splitrange);
        }

        /// <summary>
        /// Convert VSPair list to string for display
        /// Note: ranges *NOT split* into individual states
        /// </summary>
        /// <param name="StatusControl">Append state value to name</param>
        /// <returns></returns>
        public string vspsListStr(ePairStatusControl StatusControl)
        {
            //Note: ranges *NOT split* into individual states
            MyPairList pairs = vspsList(StatusControl, SplitRangeType.NotSplit);
            List<string> states = new List<string>();
            string currentValStr = this.Value.ToString();

            foreach (MyPair pair in pairs)
            {
                if (!states.Contains(pair.Name))
                {
                    if (currentValStr == pair.ValueStr)
                        states.Add($"<b>{pair.Name}</b>");
                    else
                        states.Add(pair.Name);
                }
            }

            return String.Join(", ", states);
        }


        /// <summary>
        /// Find VSPair matching MyPair min-max range
        /// </summary>
        /// <param name="myPair"></param>
        /// <returns></returns>
        public VSPair FindVSPair(MyPair myPair)
        {
            return statusPairsDict.FindVSPair(myPair);
        }

        #endregion VSPairs


        #region Methods

        /// <summary>
        /// Log
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="error"></param>
        public virtual void Log(string msg, bool error = false, bool warning = false)
        {
            if (log_enable || error || warning)
            {
                msg = $"[{RefId}]: {msg}";
                Console.WriteLine(msg); // TEMP - TODO: only if console visible?
                Utils.LogType type = error ? Utils.LogType.Error : warning ? Utils.LogType.Warning : Utils.LogType.Normal;
                Utils.Log(msg, type);

                if (error)
                {
                    //this.log_enable = true;
                    this.debug = true;
                }
            }
        }

        public void LogErr(string msg)
        {
            // If something wrong happens keep the error string
            Error = msg;
            Log(msg, error: true);
        }

        public void LogWarn(string msg)
        {
            Log(msg, warning: true);
        }

        public void LogDeb(string msg)
        {
            if (this.debug)
                Log("  *** " + msg);
        }


        /// <summary>
        /// This function is called from plugin SetIOMulti (for my devices) or HSEvent (other devices)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cause"></param>
        public virtual void NotifyValueChange(double value, string cause, string ControlString)
        {
        }


        /// <summary>
        /// If connection is lost - val_saved will have last "Value" (if it was set)
        /// Set it after restoring connection
        /// </summary>
        public void CheckSavedValue()
        {
            if (val_saved != null)
            {
                Value = (double)val_saved;
                val_saved = null;
            }
        }


        /// <summary>
        /// Set device Type to "Virtual", only if it's not set yet and only for my plugin
        /// </summary>
        /// <param name="deflt"></param>
        public void CheckType(string deflt = "Virtual")
        {
            string iface = Interface;
            if (String.IsNullOrEmpty(Type) && (String.IsNullOrEmpty(iface) || iface == controller.plugin.Name))
                Type = deflt;
        }

        /// <summary>
        /// Set/remove this device Interface to this plugin
        /// </summary>
        /// <param name="thisplugin"></param>
        public void SetInterface(bool thisplugin)
        {
            string used = null;
            string addremove = null;

            // Set this device Interface to this plugin
            if (this.Interface == "" && thisplugin)
            {
                used = "IS";
                addremove = "SETTING";
                this.Interface = controller.plugin.Name;
            }
            // Remove this device Interface, since it's not used anymore
            if (this.Interface == controller.plugin.Name && !thisplugin)
            {
                used = "NOT";
                addremove = "REMOVING";
                this.Interface = "";
            }

            if (addremove != null)
            {
                Log($"Device '{Name}' {used} used by '{controller.plugin.Name}' - {addremove} interface");
            }
        }



        #endregion Methods


        #region URL Methods
        

        /// <summary>
        /// Create link to config page for this device
        /// </summary>
        /// <param name="show_id">Display device id, not Name</param>
        /// <returns></returns>
        public string GetURL(bool show_id = false, string display = null)
        {
            // Display device PEDs in tooltip (title)
            string title = $"{RefId}: {FullName}\n";

            if (MySettingsBase.ShowPEDsInGetURL)
            {
                Dictionary<string, object> peds = this.GetAllPEDs();
                foreach (string name in peds.Keys)
                {
                    string val = (peds[name] != null) ? peds[name].ToString() : "";
                    title += $"{name}: {val}\n";
                }
            }

            display = display ?? (show_id ? $"{RefId}" : Name);
            string err = !String.IsNullOrEmpty(Error) ? PageBuilder.MyToolTip(Error, error: true) : "";
            return GetURL(RefId, title, err, display);
        }

        /// <summary>
        /// Create link to config page for this device
        /// </summary>
        /// <param name="RefId"></param>
        /// <param name="title">Tooltip</param>
        /// <param name="err"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        public static string GetURL(int RefId, string title, string err, string display)
        {
            return $"<a target='_self' class='device_management_name' title='{title}'" +
                            $" href='deviceutility?ref={RefId}&edit=1'>{err}{display}</a>";
        }


        /// <summary>
        /// Create path for storing images accesible from browser
        /// </summary>
        /// <param name="browser">For browser return path relative to homeseer/html/</param>
        /// <param name="appendName">If true append pluging name</param>
        /// <param name="fname">Filename optional</param>
        /// <returns></returns>
        public string GetImgPath(bool browser, bool appendName = true, string fname = null)
        {
            string path;
            string add = appendName ? controller.plugin.PluginNameNoSpace : "images";
            if (browser)
            {
                path = $"/{add}/";
                if (!String.IsNullOrEmpty(fname))
                    path = $"{path}{fname}";
            }
            else
            {
                path = $"html/{add}/";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                if (!String.IsNullOrEmpty(fname))
                    path = $"{path}{fname}";
            }

            return path;
        }

        //public static string imgPath = "C:\\Program Files (x86)\\HomeSeer HS3\\html\\images\\Devices\\";

        #endregion URL Methods


        #region ConfigDevice

        public virtual string GetDeviceConfig()
        {
            return null;
        }

        public virtual ConfigDevicePostReturn ConfigDevicePost(NameValueCollection parts)
        {
            return ConfigDevicePostReturn.CallbackOnce;
        }

        #endregion ConfigDevice
    }
}