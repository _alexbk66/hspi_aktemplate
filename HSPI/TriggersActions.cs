﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


using HomeSeerAPI;
using Scheduler;


namespace HSPI_AKTemplate
{
    #region Base Action/Trigger Data


    /// <summary>
    /// Base class for ActionDict/TriggerDict
    /// </summary>
    /// <typeparam name="T">ActionData/TriggerData</typeparam>
    [Serializable]
    public class BaseActTrigDict<T> : Dictionary<int, T> where T : BaseActTrigData<T>, new()
    {
        public ControllerBase controller;

        #region Construction

        public BaseActTrigDict(ControllerBase controller)
        {
            this.controller = controller;
        }


        public void AddData(int TANumber, T actData)
        {
            // TANumber is 1 based, so just add an empty T()
            if (this.Count == 0)
                this[0] = new T();

            actData.controller = this.controller;
            this[TANumber] = actData;
        }

        #endregion Construction


        #region Find

        public T Find(IPlugInAPI.strTrigActInfo actInfo)
        {
            T td = BaseActTrigData<T>.DeSerialize(actInfo.DataIn);

            if (td == null)
            {
                //td = Find(actInfo.TANumber);
                td = Find(actInfo.TANumber, actInfo.SubTANumber);
            }

            if (td != null)
                td.controller = this.controller;

            return td;
        }


        /// <summary>
        /// For ActionData there's no SubTANumber, so just ignore
        /// </summary>
        /// <param name="TANumber"></param>
        /// <param name="SubTANumber"></param>
        /// <returns></returns>
        public virtual T Find(int TANumber, int SubTANumber = -1)
        {
            T ad = this[TANumber];
            if (ad != null)
                ad.controller = this.controller;
            return ad;
        }


        #endregion Find


        #region Trigger/Actions HSPI Functions

        public string get_Name(int triggerNumber)
        {
            // TANumber is 1 based
            return this.Find(triggerNumber).Name;
        }


        public string get_Name(int triggerNumber, int subTriggerNumber)
        {
            return this.Find(triggerNumber, subTriggerNumber).Name;
        }


        public string BuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo triggerInfo)
        {
            if (this.Count < 2)
                return triggerInfo.Str();

            return Find(triggerInfo).BuildUI(uniqueControlId, triggerInfo);
        }


        public string FormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            if (this.Count < 2)
                return actionInfo.Str();

            return Find(actionInfo).FormatUI(actionInfo);
        }


        public IPlugInAPI.strMultiReturn ProcessPostUI(NameValueCollection postData,
            IPlugInAPI.strTrigActInfo actionInfo)
        {
            return Find(actionInfo).ProcessPostUI(postData, actionInfo);
        }

        #endregion Trigger/Actions HSPI Functions
    }


    /// <summary>
    /// Base class for ActionData/TriggerData
    /// </summary>
    /// <typeparam name="T">ActionData/TriggerData</typeparam>
    [Serializable]
    public class BaseActTrigData<T> where T : class
    {
        public int TANumber;
        // Note: SubTANumber can be -1 if user didn't select SubTrigger from combobox
        public int SubTANumber;


        [NonSerialized]
        public ControllerBase controller;


        public string Name { get; protected set; }


        #region Construction

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="TANumber"></param>
        /// <param name="Name"></param>
        /// <param name="controller"></param>
        public BaseActTrigData(int TANumber = 0, string Name = null, ControllerBase controller = null)
        {
            this.TANumber = TANumber;
            this.Name = Name;
            this.controller = controller;
        }

        /// <summary>
        /// Instance of TriggerData (i.e. 'this') is stored in triggerInfo.DataIn
        /// See also TriggerData.DeSerialize
        /// </summary>
        /// <param name="actionInfo"></param>
        public void Serialize(ref IPlugInAPI.strTrigActInfo actionInfo)
        {
            Utils.SerializeObject(this, out actionInfo.DataIn);
        }

        /// <summary>
        /// Instance of TriggerData (i.e. 'this') is stored in triggerInfo.DataIn
        /// See also TriggerData.Serialize
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static T DeSerialize(byte[] data)
        {
            Utils.DeSerializeObject(ref data, out object objOut);
            if (objOut != null)
            {
                return objOut as T;
            }
            return null;
        }


        /// <summary>
        /// Create strMultiReturn instance for ProcessPostUI return
        /// Here the new TriggerData instance is created via Serialization
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public IPlugInAPI.strMultiReturn MakeReturn(IPlugInAPI.strTrigActInfo actionInfo, string error = null)
        {
            // Here the new TriggerData instance is created via Serialization
            Serialize(ref actionInfo);

            return new IPlugInAPI.strMultiReturn()
            {
                sResult = error,
                TrigActInfo = actionInfo,
                DataOut = actionInfo.DataIn
            };
        }


        #endregion Construction


        #region Trigger/Actions HSPI Functions


        public virtual string BuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo triggerInfo)
        {
            return null;
        }

        public virtual string FormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            return null;
        }

        public virtual IPlugInAPI.strMultiReturn ProcessPostUI(NameValueCollection postData,
            IPlugInAPI.strTrigActInfo actionInfo)
        {
            return new IPlugInAPI.strMultiReturn();
        }

        public virtual bool get_Configured()
        {
            return false;
        }

        public virtual bool Execute(IPlugInAPI.strTrigActInfo actionInfo)
        {
            return false;
        }

        #endregion Trigger/Actions HSPI Functions


        #region UI Helpers

        /// <summary>
        /// Map control name (incl. uniqueControlId) to variable name for ProcessPostUI
        /// </summary>
        static VariableMap variableMap = new VariableMap();

        /// <summary>
        /// Makes unique key for the static dictionaries above
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        string MakeKey(string Name)
        {
            return $"{Name.ToLower()}_{TANumber}_{SubTANumber}";
        }

        /// <summary>
        /// Creates FormCheckBox  and map it's id to variable name
        /// </summary>
        /// <param name="Name">Both variable name and control name</param>
        /// <param name="uniqueControlId">From BuildUI, to append to Name</param>
        /// <param name="Checked"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public string FormCheckBox(string Name, string uniqueControlId, bool Checked, string label = "")
        {
            string id = variableMap.SetName<bool>(Name, uniqueControlId, this, MakeKey);
            return PageBuilder.FormCheckBox(id, "events", Checked, label, true, true);
        }


        /// <summary>
        /// Create TextBox and map it's id to variable name
        /// </summary>
        /// <param name="Name"><Both variable name and control name/param>
        /// <param name="uniqueControlId">From BuildUI, to append to Name</param>
        /// <param name="prompt"></param>
        /// <param name="value"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public string FormTextBox(string Name, string uniqueControlId, string prompt, string value, int size = 0)
        {
            string id = variableMap.SetName<string>(Name, uniqueControlId, this, MakeKey);
            return PageBuilder.FormTextBox_(id, "events", value, prompt, "", size);
        }


        /// <summary>
        /// Create DropDown and map it's id to variable name
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="uniqueControlId">From BuildUI, to append to Name</param>
        /// <param name="Options">MyPairList</param>
        /// <param name="SelectedValue"></param>
        /// <param name="width"></param>
        /// <param name="AddBlankRow"></param>
        /// <returns></returns>
        public string FormDropDown<U>(string Name, string uniqueControlId, ref MyPairList Options, U SelectedValue, int width = 150, bool? AddBlankRow = null, string blankText = "")
        {
            string _SelectedValue = $"{SelectedValue}";
            Type t = typeof(U);
            // Add blank row if nothing is selected yet
            if (AddBlankRow == null)
                AddBlankRow = String.IsNullOrEmpty(_SelectedValue) || _SelectedValue == $"{default(U)}";

            string id = variableMap.SetName<U>(Name, uniqueControlId, this, MakeKey);
            return PageBuilder.FormDropDown(id, "events", ref Options, -1, out MyPair _, width, true, (bool)AddBlankRow, true, "", true, blankText, _SelectedValue);
        }

        public string FormDropDown(string Name, string uniqueControlId, ref Dictionary<string, string> Options, string SelectedValue, int width = 150, bool? AddBlankRow = null, string blankText = "")
        {
            MyPairList Options1 = new MyPairList(Options);
            return FormDropDown<string>(Name, uniqueControlId, ref Options1, SelectedValue, width, AddBlankRow, blankText);
        }

        /// <summary>
        /// Checks if control name is stored in Dictionary map and sets the mapped variable
        /// </summary>
        /// <param name="postData"></param>
        protected bool CheckVariables(NameValueCollection postData)
        {
            variableMap.CheckVariables(postData, this, MakeKey);
            return false;
        }

        #endregion UI Helpers
    }


    #endregion Base Action/Trigger Data


    #region Action Data

    [Serializable]
    public class ActionDict : BaseActTrigDict<ActionData>
    {

        public ActionDict(ControllerBase controller = null) : base(controller)
        {
        }

    }


    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ActionData : BaseActTrigData<ActionData>
    {


        public ActionData(int TANumber = 0, string Name = null, ControllerBase controller = null)
            : base(TANumber, Name, controller)
        {
        }

        public ActionData() : this(0, null, null)
        {
        }
    }


    #endregion Action Data


    #region Trigger Data


    /// <summary>
    /// Dictionary mapping TANumber to TriggerData
    /// </summary>
    [Serializable]
    public class TriggerDict : BaseActTrigDict<TriggerData>
    {

        public TriggerDict(ControllerBase controller) : base(controller)
        {
        }


        public override TriggerData Find(int TANumber, int SubTANumber = -1)
        {
            TriggerData td = this[TANumber][SubTANumber];
            if (td != null)
                td.controller = this.controller;
            return td;
        }
    }



    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TriggerData : BaseActTrigData<TriggerData>
    {
        // Does it support condition?
        public bool Condition { get; protected set; }

        // Salled from HS when user configres Event - if it's a trigger or a condition
        public bool IsCondition { get; set; }

        /// <summary>
        /// List of TriggerData subbtriggers
        /// </summary>
        public List<TriggerData> subbtriggers { get; }

        #region Construction

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="TANumber"></param>
        /// <param name="Name"></param>
        /// <param name="condition"></param>
        /// <param name="controller"></param>
        public TriggerData( int TANumber = 0,
                            string Name = null,
                            bool condition = false,
                            ControllerBase controller = null,
                            bool RegisterCallback = false)
            : base(TANumber, Name, controller)
        {
            this.SubTANumber = -1;
            this.Condition = condition;

            subbtriggers = new List<TriggerData>();

            if (controller != null && RegisterCallback)
                controller.EventCallback += InvokeEventCallback;
        }

        public TriggerData() : this(0, null, false, null)
        {
        }

        public TriggerData this[int subTriggerNumber]
        {
            get
            {
                // TANumber/SubTANumber is 1 based
                if (subTriggerNumber < 1 || subTriggerNumber >= subbtriggers.Count)
                    return this;
                else
                    return subbtriggers[subTriggerNumber];
            }
        }

        #endregion Construction


        #region Trigger HSPI Functions

        public string get_SubTriggerName(int subTriggerNumber)
        {
            return this[subTriggerNumber].Name;
        }

        public int get_SubTriggerCount()
        {
            // TANumber/SubTANumber is 1 based
            return subbtriggers.Count - 1;
        }


        /// <summary>
        /// This is called from HSPI.TriggerTrue
        /// </summary>
        /// <returns></returns>
        public virtual bool TriggerTrue()
        {
            return TriggerTrue(null, null);
        }

        /// <summary>
        /// This is called from InvokeEventCallback
        /// </summary>
        /// <param name="source"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual bool TriggerTrue(object source, object data)
        {
            Console.WriteLine($"Trigger {this} - {source} - {data}");
            return false;
        }

        #endregion Trigger HSPI Functions

        /// <summary>
        /// Received EventCallback from Controller
        /// Call TriggerTrue to chack if Trigger conditions are satisfied
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ev"></param>
        public virtual void InvokeEventCallback(object source, object data)
        {
            if (controller == null)
            {
                Console.WriteLine("TriggerData.InvokeEventCallback: 'controller' is NULL");
                return;
            }
            if (controller.plugin == null)
            {
                Console.WriteLine("TriggerData.InvokeEventCallback: 'controller.plugin' is NULL");
                return;
            }
            if (controller.plugin.Callback == null)
            {
                Console.WriteLine("TriggerData.InvokeEventCallback: 'controller.plugin.Callback' is NULL");
                return;
            }

            IAppCallbackAPI Callback = controller.plugin.Callback;

            // Get all triggers matching TANumber, SubTANumber from HS
            IPlugInAPI.strTrigActInfo[] trigs = Callback.TriggerMatches(controller.plugin.Name, TANumber, SubTANumber);
            if (trigs == null)
            {
                Console.WriteLine("TriggerData.InvokeEventCallback: 'trigs' is NULL");
                return;
            }
 
            foreach (IPlugInAPI.strTrigActInfo trig in trigs)
            {
                TriggerData td = DeSerialize(trig.DataIn);
                if (td == null)
                    continue;

                td.controller = controller;

                // Call virtual TriggerTrue to chack if Trigger conditions are satisfied
                if (td.TriggerTrue(source, data))
                {
                    controller.SetGlobalVars(source, data);
                    // Fire!
                    Callback.TriggerFire(controller.plugin.Name, trig);
                }
            }
        }
    }


    #endregion Trigger Data
}
