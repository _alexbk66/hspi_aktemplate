﻿using System;
using System.Web;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

using HomeSeerAPI;
using Scheduler;


namespace HSPI_AKTemplate
{

    public static class XX
    {
        public static string Str(this IPlugInAPI.strTrigActInfo actionInfo)
        {
            string str = $"TrigActInfo Ev {actionInfo.evRef}, UID {actionInfo.UID}, TAN {actionInfo.TANumber}";
            if(actionInfo.SubTANumber != 0)
                str += $", SubTAN {actionInfo.SubTANumber}";
            return str;
        }
    }



    public abstract partial class HspiBase2
    {

        #region Action

        ActionDict actionDict;


        /// <summary> The number of actions the plugin supports. </summary>
        public override int ActionCount()
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            if (actionDict == null)
                return 0;
            // TANumber is 1 based
            return actionDict.Count - 1;
        }

        /// <summary>
        /// Return the name of the action given an action number. The name of the action will be displayed in the HomeSeer events actions list.
        /// </summary>
        /// <param name="ActionNumber">The number of the action. Each action is numbered, starting at 1.</param>
        /// <returns>Name of the action.</returns>
        public override string get_ActionName(int actionNumber)
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            // TANumber is 1 based
            return actionDict.get_Name(actionNumber);
        }


        /// <summary>
        /// This function is called from the HomeSeer event page when an event is in edit mode
        /// Your plugin needs to return HTML controls so the user can make action selections.
        /// Normally this is one of the HomeSeer jquery controls such as a clsJquery.jqueryCheckbox.
        /// </summary>
        /// <param name="sUnique">A unique string that can be used with your HTML controls to identify the control. All controls need to have a unique ID.</param>
        /// <param name="ActInfo">Object that contains information about the action like current selections.</param>
        /// <returns>HTML controls that need to be displayed so the user can select the action parameters.</returns>
        public override string ActionBuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}: {actionInfo.Str()}");
            return actionDict.BuildUI(uniqueControlId, actionInfo);
        }


        /// <summary>
        /// After the action has been configured, this function is called in your plugin
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override string ActionFormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return actionDict.FormatUI(actionInfo);
        }


        /// <summary>
        /// When a user edits your event actions in the HomeSeer events, this function is called to process the selections.
        /// </summary>
        /// <param name="PostData">A collection of name value pairs that include the user's selections.</param>
        /// <param name="TrigInfoIN">Object that contains information about the action.</param>
        /// <returns>Object that holds the parsed information for the action. HomeSeer will save this information for you in the database.</returns>
        public override IPlugInAPI.strMultiReturn ActionProcessPostUI(NameValueCollection postData,
            IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return actionDict.ProcessPostUI(postData, actionInfo);
        }

        /// <summary>
        /// Return TRUE if the given action is configured properly.
        /// There may be times when a user can select invalid selections for the action and 
        /// in this case you would return FALSE so HomeSeer will not allow the action to be saved.
        /// </summary>
        /// <param name="ActInfo">Object describing the action.</param>
        public override bool ActionConfigured(IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return actionDict.Find(actionInfo).get_Configured();
        }


        /// <summary>
        /// Indicate if the given devices is referenced by the given action.
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public override bool ActionReferencesDevice(IPlugInAPI.strTrigActInfo actionInfo, int deviceId)
        {
            Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return false;
        }

        /// <summary>
        /// When an event is triggered, this function is called to carry out the selected action. 
        /// Use the ActInfo parameter to determine what action needs to be executed then execute this action.
        /// </summary>
        public override bool HandleAction(IPlugInAPI.strTrigActInfo actionInfo)
        {
            Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return actionDict.Find(actionInfo).Execute(actionInfo);
        }


        /// <summary>
        /// This function is called after controller is created from InitIO
        /// Here we build our dictionary of Action definitions
        /// </summary>
        /// <param name="TANumber"></param>
        /// <param name="trigData"></param>
        public void AddActionData(int TANumber, ActionData actData, ControllerBase controller = null)
        {
            if (actionDict == null)
                actionDict = new ActionDict(controller);

            actionDict.AddData(TANumber, actData);
        }

        #endregion Action


        #region Trigger

        TriggerDict triggerDict;

        /// <summary>
        /// Indicate if the plugin has any triggers.
        /// </summary>
        /// <returns></returns>
        protected override bool GetHasTriggers()
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            return this.GetTriggerCount() > 0;
        }

        /// <summary>
        /// <summary> Number of triggers the plugin supports.
        /// </summary>
        /// <returns></returns>
        protected override int GetTriggerCount()
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            if (triggerDict == null)
                return 0;
            // TANumber is 1 based
            return triggerDict.Count - 1;
        }

        /// <summary>
        /// Return the name of the given trigger based on the trigger number.
        /// </summary>
        /// <param name="triggerNumber"></param>
        /// <returns></returns>
        public override string get_TriggerName(int triggerNumber)
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            // TANumber is 1 based
            return triggerDict.get_Name(triggerNumber);
        }

        /// <summary>
        /// Return the HTML controls for a given trigger.
        /// </summary>
        /// <param name="uniqueControlId"></param>
        /// <param name="triggerInfo"></param>
        /// <returns></returns>
        public override string TriggerBuildUI(string uniqueControlId, IPlugInAPI.strTrigActInfo triggerInfo)
        {
            //Console.WriteLine($"\n{Utils.FuncName()}:  {triggerInfo.Str()}");
            return triggerDict.BuildUI(uniqueControlId, triggerInfo);
        }


        /// <summary>
        /// After the trigger has been configured, this function is called in your plugin
        /// to display the configured trigger.
        /// </summary>
        /// <returns>Text that describes the given trigger.</returns>
        public override string TriggerFormatUI(IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"\n{Utils.FuncName()}:  {actionInfo.Str()}");
            return triggerDict.FormatUI(actionInfo);
        }


        /// <summary>
        /// Indicate if the given trigger is configured properly.
        /// HS then decides - to call TriggerBuildUI or TriggerFormatUI
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public override bool get_TriggerConfigured(IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"\n{Utils.FuncName()}:  {actionInfo.Str()} : '{ok}'");
            return triggerDict.Find(actionInfo).get_Configured();
        }


        /// <summary>
        /// Process a post from the events web page when a user modifies any of the controls related to a plugin trigger.
        /// After processing the user selctions, create and return a strMultiReturn object.
        /// </summary>
        public override IPlugInAPI.strMultiReturn TriggerProcessPostUI(NameValueCollection postData,
            IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return triggerDict.ProcessPostUI(postData, actionInfo);
        }


        /// <summary>
        /// Indicate if the given device is referenced by the given trigger.
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public override bool TriggerReferencesDevice(IPlugInAPI.strTrigActInfo actionInfo, int deviceId)
        {
            Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return false;
        }


        /// <summary>
        /// Triggers can also be conditions, and that is where this is used.
        /// If this function is called, TrigInfo will contain the trigger information pertaining to a trigger used as a condition.
        /// When an event is triggered and it has conditions, the conditions need to be evaluated
        /// </summary>
        public override bool TriggerTrue(IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return triggerDict.Find(actionInfo).TriggerTrue();
        }


        /// <summary>
        /// Return the number of sub triggers each trigger supports.
        /// </summary>
        /// <param name="triggerNumber"></param>
        /// <returns></returns>
        public override int get_SubTriggerCount(int triggerNumber)
        {
            return triggerDict.Find(triggerNumber).get_SubTriggerCount();
        }


        /// <summary>
        /// Return the text name of the sub trigger given its trugger number and sub trigger number.
        /// </summary>
        /// <param name="triggerNumber"></param>
        /// <param name="subTriggerNumber"></param>
        /// <returns></returns>
        public override string get_SubTriggerName(int triggerNumber, int subTriggerNumber)
        {
            return triggerDict.get_Name(triggerNumber, subTriggerNumber);
        }


        /// <summary>
        /// This function is called after controller is created from InitIO
        /// Here we build our dictionary of Trigger definitions
        /// </summary>
        /// <param name="TANumber"></param>
        /// <param name="trigData"></param>
        public void AddTriggerData(int TANumber, TriggerData trigData, ControllerBase controller = null)
        {
            if(triggerDict == null)
                triggerDict = new TriggerDict(controller);

            triggerDict.AddData(TANumber, trigData);
        }

        #endregion Trigger


        #region Condition

        /*
         * Note: in the functions below if function argument is triggerNumber/subTriggerNumber
         * these functions are used for building UI for the generic trigger definitions (like template)
         *
         * If function argument is actionInfo - this function is for particular trigger instance
         * created by user for particular event
         */

        /// <summary>
        /// Set to <c>true</c> if the trigger is being used as a CONDITION.
        /// Check this value in BuildUI and other procedures to change how the trigger
        /// is rendered if it is being used as a condition or a trigger.
        /// </summary>
        public override bool get_Condition(IPlugInAPI.strTrigActInfo actionInfo)
        {
            //Console.WriteLine($"{Utils.FuncName()}:  {actionInfo.Str()}");
            return triggerDict.Find(actionInfo).Condition;
        }

        /// <summary>
        /// Called from HS when user configres Event - if it's a trigger or a condition
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <param name="value">it's a trigger or a condition</param>
        public override void set_Condition(IPlugInAPI.strTrigActInfo actionInfo, bool value)
        {
            triggerDict.Find(actionInfo).IsCondition = value;
        }

        /// <summary>
        /// Indicate if the given trigger can also be used as a condition for the given grigger number.
        /// </summary>
        /// <param name="triggerNumber"></param>
        /// <returns></returns>
        public override bool get_HasConditions(int triggerNumber)
        {
            //Console.WriteLine($"{Utils.FuncName()}: ");
            if(triggerNumber <= GetTriggerCount())
                return triggerDict[triggerNumber].Condition;

            return false;
        }

        #endregion Condition
    }

}
