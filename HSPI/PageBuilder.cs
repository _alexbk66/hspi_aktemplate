﻿// Copyright (C) 2016 SRG Technology, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;

using HomeSeerAPI;
using Scheduler;
using static Scheduler.clsJQuery;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Reflection;
using System.Web;
using System.Collections;

namespace HSPI_AKTemplate
{

    /// <summary>
    /// This class adds some common support functions for creating the web pages used by HomeSeer plugins. 
    /// <para/>For each control there are three functions:  
    /// <list type="bullet">
    /// <item><description><c>Build:</c> Used to initially create the control in the web page.</description></item>
    /// <item><description><c>Update:</c> Used to modify the control in an existing web page.</description></item>
    /// <item><description><c>Form:</c> Not normally call externally but could be useful in special circumstances.</description></item>
    /// </list>
    /// </summary>
    /// <seealso cref="Scheduler.PageBuilderAndMenu.clsPageBuilder" />
    public partial class PageBuilder : PageBuilderAndMenu.clsPageBuilder
    {
        /// <summary>
        /// Name of "deviceutility" page - device config page
        /// </summary>
        public static readonly string DEVICEUTILITY = "deviceutility";

        /// <summary>
        /// PageBuilder Constructor.
        /// </summary>
        /// <param name="pagename">The name used by HomeSeer when referencing this particular page.</param>
        /// <param name="plugin"></param>
        /// <param name="register">Don't RegisterWebPage for "deviceutility" page</param>
        /// <param name="config">RegisterConfigLink for "manage Interfaces" page</param>
        public PageBuilder(string pagename, HspiBase2 plugin, bool register = true, bool config = false)
            //: base( plugin.MakePageLink(pagename)) // clsPageBuilder by pageName actually means link
            : base(pagename) // or not?
        {
            UsesJqAll = false;
            PageLink = PageName;

            this.plugin = plugin;

            if (plugin != null)
            {
                gLocLabel = hs.GetINISetting("Settings", "gLocLabel", "");
                gLocLabel2 = hs.GetINISetting("Settings", "gLocLabel2", "");
                bUseLocation2 = "True" == hs.GetINISetting("Settings", "bUseLocation2", "");
                bLocationFirst = "True" == hs.GetINISetting("Settings", "bLocationFirst", "");
                if (bLocationFirst)
                    LocationLabels = Tuple.Create(gLocLabel, gLocLabel2);
                else
                    LocationLabels = Tuple.Create(gLocLabel2, gLocLabel);
            }

            if (register || config)
            {
                RegisterWebPage(config);
            }
        }

        /// <summary>
        /// Register the page Link for Plugins Menu
        /// </summary>
        /// <param name="config">Also RegisterConfigLink for "manage Interfaces" page</param>
        /// <returns></returns>
        public string RegisterWebPage(bool config = false)
        {
            try
            {
                // Store for creating clsJQuery controls
                this.PageLink = plugin.RegisterWebPage(this.PageName, type: config ? HspiBase2.PageType.Both : HspiBase2.PageType.Menu);

                registerPage(PageLink, this);
                registerPage("/" + PageLink, this);

                return PageLink;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Registering Web Links (RegisterWebPage): " + ex.Message);
                return null;
            }
        }


        #region MemberVariables

        //public const char id_prefix = '_';

        protected HspiBase2 plugin;
        public IHSApplication hs { get { return plugin.HS; } }

        // From HS configuration
        protected string gLocLabel = null;
        protected string gLocLabel2 = null;
        protected bool bUseLocation2 = false;
        public static bool bLocationFirst = false;
        public static Tuple<string, string> LocationLabels = null;

        // Webpage link created by Utils.RegisterWebPage()
        public string PageLink { protected set; get; }

        #endregion MemberVariables

        #region virtual members

        /// <summary>
        /// Measure Duration of BuildContent() call
        /// </summary>
        StopwatchEx watch;

        public long duration { get => watch.Stop(); }

        public static string GetPagePlugin(string page, string user, int userRights, string queryString)
        {
            // Get existing page from Utils dictionary
            PageBuilder pb = (PageBuilder)findRegisteredPage(page);

            if (pb == null)
                return "Page not found: '" + page + "'";

            // If called from GenPage - don't add header/footer, it will be appended
            bool addHeaderFooter = !page.StartsWith("/");

            // See TableBuilder.BuildHeaderLink for queryString format
            NameValueCollection queryPairs = queryString != null ? HttpUtility.ParseQueryString(queryString) : new NameValueCollection();

            return pb.GetHTML(addHeaderFooter, queryPairs);
        }

        /// <summary>
        /// Add <div> for HS to display errors
        /// </summary>
        /// <returns></returns>
        string MakeErrDiv()
        {
            var stb = new StringBuilder();
            stb.Append(DivStart("errormessage", "class='errormessage'"));
            stb.Append(DivEnd());

            return stb.ToString();
        }

        /// <summary>
        /// Main functions which generates page HTML
        /// Calls virtual BuildContent function
        /// </summary>
        /// <returns></returns>
        public virtual string GetHTML(bool addHeaderFooter = false, NameValueCollection queryPairs = null)
        {
            watch = new StopwatchEx(PageName);
            reset();
            suppressDefaultFooter = true;

            string html = MakeErrDiv();

            //////////////////////////////
            html += BuildContent(queryPairs);
            //////////////////////////////

            // Must add form for POSTback to work
            html = FormStart("addForm", PageName.Replace(" ", ""), "POST") + html + FormEnd();

            if (addHeaderFooter)
            {
                string title = plugin.Name + " " + PageName + ""; // Instance
                string header = hs.GetPageHeader(PageName, title, "", "", false, false);
                AddHeader(header);

                //////////////////////////////
                AddBody(html);
                //////////////////////////////

                AddFooter(hs.GetPageFooter());
            }

            //Utils.Log($"Duration '{PageName}': {duration} ms.");
            if (addHeaderFooter)
                html = BuildPage();

            return html;
        }

        /// <summary>
        /// Virtual function for creating page body
        /// Should be overidden by derived classes
        /// </summary>
        /// <returns></returns>
        public virtual string BuildContent(NameValueCollection queryPairs = null)
        {
            return "";
        }

        public static string PostBackProc(string page, string data, string user, int userRights)
        {
            // Get existing page from Utils dictionary
            PageBuilder pb = (PageBuilder)findRegisteredPage(page);

            if (pb == null)
                return "Page not found: '" + page + "'";

            return pb.postBackProc(page, data, user, userRights);
        }

        public override string postBackProc(string page, string data, string user, int userRights)
        {
            // See TableBuilder.BuildHeaderLink for queryString format
            NameValueCollection queryPairs = HttpUtility.ParseQueryString(data);

            this.PostBackProc(queryPairs);

            return base.postBackProc(page, data, user, userRights);
        }

        public virtual string PostBackProc(NameValueCollection queryPairs)
        {
            return "";
        }


        #endregion virtual members


        #region Page Timer

        public void StartPageTimer(int tm = 3000)
        {
            this.RefreshIntervalMilliSeconds = tm;
            if(tm==0)
                this.pageCommands.Add("stoptimer", "");
            else
                this.pageCommands.Add("starttimer", "");
        }

        /// <summary>
        /// Function for initiating Ajax timer and creating <div> for the result
        /// Timer PostBackProc will receive the pair, i.e. "if (parts["action"] == "div_name")
        /// </summary>
        /// <param name="div_name">Name of div</param>
        /// <param name="action">key name will be passed to PostBackProc"</param>
        /// <param name="init">Initial string to set in div</param>
        /// <param name="tm">timer interval in ms</param>
        /// <returns></returns>
        protected string AddTimerDiv(string div_name = "timer_div", string action = "action", string init = "", int tm = 2000)
        {
            this.RefreshIntervalMilliSeconds = tm;

            action_div_dict[div_name] = action;

            string html = this.AddAjaxHandlerPost($"{action}={div_name}", PageLink);
            html += $"<div id='{div_name}'>{init}</div>\r\n";
            return html;
        }

        /// <summary>
        /// Dictionary mapping div_name to action
        /// </summary>
        Dictionary<string, string> action_div_dict = new Dictionary<string, string>();

        /// <summary>
        /// Check if it's timer callbac (ajax post)
        /// i.e. if (parts["action"] == "div_name")
        /// </summary>
        /// <param name="parts"></param>
        /// <param name="div_name"></param>
        /// <param name="action"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected bool CheckTimerCallback(NameValueCollection parts, string div_name = "timer_div", string action = null, object value = null)
        {
            if (parts == null)
                return false;

            if (String.IsNullOrEmpty(action))
            {
                if (!action_div_dict.ContainsKey(div_name))
                    return false;

                action = action_div_dict[div_name];
            }

            if (parts[action] == div_name)
            {
                // ajax timer has expired and posted back to us, update the time
                if(value!=null)
                    this.divToUpdate.Add(div_name, value.ToString());
                return true;
            }

            return false;
        }

        #endregion Page Timer
    }
}