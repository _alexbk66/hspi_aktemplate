﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSPI_AKTemplate
{
    public partial class PageBuilder
    {
        /// <summary>
        /// Map control name (incl. uniqueControlId) to variable name for ProcessPostUI
        /// </summary>
        static VariableMap variableMap = new VariableMap();


        /// <summary>
        /// Map variable name to control name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name">Variable name</param>
        /// <param name="uniqueControlId"></param>
        /// <param name="parent">Object containing the variable/property</param>
        /// <param name="_MakeKey"></param>
        /// <returns></returns>
        public string AddVariableMap<T>(string Name, object parent = null, Func<string, string> _MakeKey = null)
        {
            if (parent == null)
                parent = this;

            return variableMap.SetName<T>(Name, unique(parent), parent, _MakeKey);
        }

        /// <summary>
        /// Map variable name to control name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name">Variable name</param>
        /// <param name="UniqueCtrlName"></param>
        /// <param name="parent">Object containing the variable/property</param>
        /// <returns></returns>
        public string AddVariableMap<T>(string Name, object parent)
        {
            if (parent == null)
                parent = this;

            return variableMap.SetName<T>(Name, unique(parent), parent);
        }

        /// <summary>
        /// Make mapping ID unique as the map is static, not member
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        static string unique(object parent)
        {
            return (parent is DeviceBase) ? (parent as DeviceBase).RefId.ToString() : parent.GetHashCode().ToString();
        }


        /// <summary>
        /// Map table slider control to bool variable open/close
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="tableID"></param>
        /// <param name="parent">Object containing the variable/property</param>
        /// <returns></returns>
        public string AddSliderMap(string Name, string tableID, object parent)
        {
            if (parent == null)
                parent = this;
            string UniqueCtrlName = TableBuilder.SliderName(tableID);
            Variable<bool> map = variableMap.SetName<bool>(Name, tableID, UniqueCtrlName, parent);
            map.table_slider = true;
            return UniqueCtrlName;
        }


        /// <summary>
        /// See variableMap.CheckVariables
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="parent1"></param>
        public bool CheckVariables(NameValueCollection postData, object parent1 = null)
        {
            return variableMap.CheckVariables(postData, parent1);
        }

        /// <summary>
        /// See variableMap.CheckVariable
        /// </summary>
        /// <param name="CtrlName"></param>
        /// <param name="StrVal"></param>
        /// <param name="parent1"></param>
        /// <returns></returns>
        public bool CheckVariable(string CtrlName, string StrVal, NameValueCollection postData = null, object parent1 = null)
        {
            if (CtrlName.StartsWith("radio_") && postData != null)
            {
                ProcessRadioBtn(ref CtrlName, ref StrVal, postData);
            }
            return variableMap.CheckVariable(CtrlName, StrVal, parent1);
        }


        /// <summary>
        /// Add CheckBox and register mapping parent.var_name
        /// </summary>
        /// <param name="val">Selected?</param>
        /// <param name="var_name">Name of mapped variable</param>
        /// <param name="parent">Oject conatining mapped variable</param>
        /// <returns></returns>
        protected string FormCheckBox(bool val, string var_name, object parent)
        {
            string id = AddVariableMap<bool>(var_name, parent);
            return FormCheckBox(id, val);
        }


        /// <summary>
        /// Add a table row with three cells: Name, Value, Comment
        /// and register mapping parent.var_name
        /// </summary>
        /// <param name="name">Name of the parameter (display)</param>
        /// <param name="val">Selected?</param>
        /// <param name="comment">comment</param>
        /// <param name="var_name">Name of mapped variable</param>
        /// <param name="parent">Oject conatining mapped variable</param>
        public void AddCheckBoxRow(TableBuilder table, string name, bool val, string var_name, object parent, string comment)
        {
            table.AddRow();
            table.AddCell(name);
            table.AddCell(FormCheckBox(val, var_name, parent));
            table.AddCell(comment);
        }


        /// <summary>
        /// BuildDropList and register mapping parent.var_name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name"></param>
        /// <param name="Options"></param>
        /// <param name="SelectedValue"></param>
        /// <param name="var_name"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected string BuildDropList<T>(string[,] Options, string SelectedValue, string var_name, object parent, int width = 150)
        {
            string id = AddVariableMap<T>(var_name, parent);
            return BuildDropList(id, Options, out MyPair _, SelectedValue: SelectedValue, width: width);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="step"></param>
        /// <param name="sel"></param>
        /// <param name="suff"></param>
        /// <param name="var_name"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected string BuildDropList<T>(double from, double to, double step, double? sel, string suff, string var_name, object parent, int width = 150)
        {
            string[,] pairs = MakePairsArray(from, to, step, suff);
            return BuildDropList<T>(pairs, $"{sel}", var_name, parent, width: width);
        }


        /// <summary>
        /// Add a table row with three cells: Name, Value, Comment
        /// Create a row with list of values from-to with step
        /// See comment for BuildDropList()
        /// </summary>
        /// <param name="name">Name of the parameter (display)</param>
        /// <param name="var_name">Variable name to map</param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="step"></param>
        /// <param name="sel">Selected value</param>
        /// <param name="suff">Suffix to add</param>
        /// <param name="comment"></param>
        public void AddDropListRow<T>(TableBuilder table, string name, string var_name, object parent, double from, double to, double step, double? sel, string suff, string comment)
        {
            string[,] pairs = MakePairsArray(from, to, step, suff);
            AddDropListRow<T>(table, name, pairs, $"{sel}", var_name, parent, comment);
        }


        /// <summary>
        /// Add a table row with three cells: Name, Value, Comment
        /// See comment for BuildDropList()
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table">TableBuilder</param>
        /// <param name="name">Name of the parameter (display)</param>
        /// <param name="Options"></param>
        /// <param name="selected">Selected value</param>
        /// <param name="var_name">Variable name to map</param>
        /// <param name="comment"></param>
        public void AddDropListRow<T>(TableBuilder table, string name, string[,] Options, string selected, string var_name, object parent, string comment)
        {
            table.AddRow();
            table.AddCell(name);
            table.AddCell(BuildDropList<T>(Options, selected, var_name, parent));
            table.AddCell(comment);
        }


        protected string FormTextBox<U>(U DefaultText, string dialogCaption, string promptText, bool SubmitForm, string var_name, object parent, int size = 0, bool number = false)
        {
            string id = AddVariableMap<U>(var_name, parent);
            return FormTextBox(id, DefaultText.ToString(), dialogCaption, promptText, SubmitForm: SubmitForm, size: size, style: number ? "number" : "");
        }


        /// <summary>
        /// Add a table row with three cells: Name, Value, Comment
        /// and register mapping parent.var_name
        /// </summary>
        /// <param name="table">TableBuilder</param>
        /// <param name="name">Name of the parameter (display)</param>
        /// <param name="value">DefaultText</param>
        /// <param name="var_name">Name of mapped variable</param>
        /// <param name="parent">Oject conatining mapped variable</param>
        /// <param name="dialogCaption"></param>
        /// <param name="promptText"></param>
        /// <param name="comment"></param>
        /// <param name="SubmitForm"></param>
        public void AddTextBoxRow(TableBuilder table, string name, string value, string var_name, object parent, string dialogCaption, string promptText, string comment = "", bool SubmitForm = false)
        {
            table.AddRow();
            table.AddCell(name);
            table.AddCell(FormTextBox(value, dialogCaption, promptText, SubmitForm, var_name, parent));
            table.AddCell(comment);
        }


        protected string FormTimePicker(TimeSpan value, string var_name, object parent, string Label = "", bool showSeconds = false)
        {
            string id = AddVariableMap<TimeSpan>(var_name, parent);
            return FormTimePicker(id, Label, showSeconds, value, true, null);
        }


        /// <summary>
        /// Generate jqRadioButton for given Enum type
        /// </summary>
        /// <param name="en">The Enum value</param>
        /// <param name="var_name">Name of mapped variable</param>
        /// <param name="parent">Oject conatining mapped variable</param>
        /// <param name="names">If passed, use names from this array, instaed of Enum names</param>
        /// <param name="style">Display style, i.e. Button or Radio</param>
        /// <param name="supported">Limit List of Enum values which should be displayed</param>
        /// <returns></returns>
        protected string RadioButtonEnum<TEnum>(Enum en, string var_name, object parent, string[] names = null, string style = null, object[] supported = null)
                where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            //name = $"radio_{name}";
            string id = AddVariableMap<TEnum>(var_name, parent);
            return RadioButtonEnum(en, id, enabled: true, names: names, style: style, supported: supported);
        }

        // Convert TEnum[] to object[]
        protected string RadioButtonEnum<TEnum>(Enum en, string var_name, object parent, string[] names = null, string style = null, TEnum[] supported = null)
                where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            object[] supported2 = null;
            if (supported != null)
            {
                // Convert TEnum[] to object[]
                List<object> supported1 = new List<object>();
                foreach (TEnum m in supported)
                    supported1.Add(m);
                supported2 = supported1.ToArray();
            }

            return RadioButtonEnum<TEnum>(en, var_name, parent, names, style, supported2);
        }

        protected string RadioButtonEnum<TEnum>(Enum en, string var_name, object parent, string[] names = null, string style = null)
                where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            return RadioButtonEnum<TEnum>(en, var_name, parent, names, style, supported: (object[])null);
        }
    }
}