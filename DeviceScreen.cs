﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using HomeSeerAPI;
using HSPI_AKTemplate;
using static HomeSeerAPI.Enums;

namespace HSPI_AKExample
{
    public class DeviceScreen : DeviceBase
    {
        public DeviceScreen(Controller controller, int devID = 0) : base(controller, devID)
        {
            Create();
        }

        public override bool Create()
        {
            bool created = Create("Screen", force: false);
            AddVSPair(0, "Off", ePairControlUse._Off, "/images/HomeSeer/status/off.gif");
            AddVSPair(100, "On", ePairControlUse._On, "/images/HomeSeer/status/on.gif");
            return created;
        }

        public override void NotifyValueChange(double value, string cause, string ControlString)
        {
            switch(value)
            {
                case 0:
                    MonitorOff();

                    // This should Trigger "SomeTriggerData"
                    object SomeData = value; // For example
                    // See SomeTriggerData.TriggerTrue()
                    (controller as Controller).InvokeEventCallback(source: this, data: SomeData);

                    break;
                case 100:
                    MonitorOn();
                    break;
            }
        }


        #region ScreenOnOff

        private static int WM_SYSCOMMAND = 0x0112;
        private static uint SC_MONITORPOWER = 0xF170;

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern void mouse_event(Int32 dwFlags, Int32 dx, Int32 dy, Int32 dwData, UIntPtr dwExtraInfo);

        private const int WmSyscommand = 0x0112;
        private const int ScMonitorpower = 0xF170;
        private const int MonitorShutoff = 2;
        private const int MouseeventfMove = 0x0001;

        public void MonitorOff()
        {
            SendMessage(GetConsoleWindow(), WM_SYSCOMMAND, (IntPtr)SC_MONITORPOWER, (IntPtr)2);
        }

        public void MonitorOn()
        {
            mouse_event(MouseeventfMove, 0, 1, 0, UIntPtr.Zero);
            Thread.Sleep(40);
            mouse_event(MouseeventfMove, 0, -1, 0, UIntPtr.Zero);
        }

        #endregion ScreenOnOff


        #region Config

        DeviceConfig config;


        public override string GetDeviceConfig()
        {
            config = new DeviceConfig(this, controller.plugin as HSPI);

            return config.GetHTML();
        }


        public override ConfigDevicePostReturn ConfigDevicePost(NameValueCollection parts)
        {
            return config.ConfigDevicePost(parts);
        }


        #endregion Config
    }


    /// <summary>
    /// Config class for 
    /// </summary>
    public class DeviceConfig : PageBuilder
    {
        private const string pageName = "deviceutility";
        DeviceScreen device;

        static bool table_slider_open = true;

        /// <summary>
        /// Constructor for CalendarPluginRootDevice
        /// </summary>
        /// <param name="device">CalendarPluginRootDevice</param>
        /// <param name="plugin"></param>
        public DeviceConfig(DeviceScreen device, HSPI plugin) : base(pageName, plugin, register: false)
        {
            this.device = device;
        }


        public override string BuildContent(NameValueCollection queryPairs = null)
        {
            string[] hdr = { "Property", "Value", "Comment" + MyToolTip("Just an example")};
            var table = new TableBuilder($"Screen Device Configuration",
              ncols: hdr.Length,
              page_name: PageName,
              tableID: "table_config",
              inSlider: true
              );

            table.AddBlankRow();
            table.AddRowHeader(hdr);
            table.AddRow();
            table.AddCell("Log");
            table.AddCell(FormCheckBox("log", device.log_enable));
            table.AddCell("Enable logging for the device");

            return table.Build(initiallyOpen: table_slider_open);
        }


        public ConfigDevicePostReturn ConfigDevicePost(NameValueCollection parts)
        {
            // Check which control "id" caused postback
            string ctrlID = parts.Get("id");

            if (ctrlID == null)
            {
                foreach (string key in parts.Keys)
                {
                    string value = parts[key];

                    // TEMP
                    //Console.WriteLine($"{key}: {value}");

                    // Check for Credentilas table slider
                    if (TableBuilder.TrySliderSet(key, value, out string tableID, out bool? state))
                    {
                        if (tableID == "table_config" && table_slider_open != state)
                        {
                            table_slider_open = (bool)state;
                            break;
                        }
                    }
                }
            }
            else
            {
                string value = parts[ctrlID];

                if (ctrlID == "log")
                {
                    bool log = (bool)Utils.StrToBool(value);
                    device.log_enable = log;
                }
            }

            return ConfigDevicePostReturn.CallbackOnce;
        }


    }
}
