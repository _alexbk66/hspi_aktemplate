﻿using HomeSeerAPI;
using HSPI_AKTemplate;

namespace HSPI_AKExample
{
    public class Settings : MySettingsBase
    {
        public Settings(string IniFile, IHSApplication Hs) : base(IniFile, Hs)
        {
        }

        public override void Load()
        {
            //UseGroupDevice = LoadVal(nameof(UseGroupDevice), false);
            //SaveNewChromecast = LoadVal(nameof(SaveNewChromecast), true);
        }

        public override void Save()
        {
            //SaveVal(nameof(UseGroupDevice), UseGroupDevice);
            //SaveVal(nameof(SaveNewChromecast), SaveNewChromecast);

            base.Save();
        }

    }
}