﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HSPI_AKTemplate;

namespace HSPI_AKExample
{
    public class Controller : ControllerBase
    {
        public DeviceScreen device;


        public Controller(HSPI plugin) : base(plugin)
        {
            plugin.AddActionData((int)Actions.screenOff, new ScreenOffAction(this), this);
            plugin.AddTriggerData((int)Triggers.SomeTrigger, new SomeTriggerData(condition: true, controller: this), this);
        }

        public override void CheckAndCreateDevices()
        {
            device = new DeviceScreen(this);
        }


        /// <summary>
        /// Action
        /// </summary>
        public void ScreenOff()
        {
            device.MonitorOff();
        }



    }
}
