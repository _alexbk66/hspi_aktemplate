﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

using HSPI_AKTemplate;
using HomeSeerAPI;
using Scheduler;
using static Scheduler.PageBuilderAndMenu;

namespace HSPI_AKExample
{
    using DeviceDict = Dictionary<int, DeviceBase>;


    class StatusPage : PageBuilder
    {
        private static string pageName = "StatusPage";

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusPage"/> class.
        /// </summary>
        /// <param name="plugin">The HomeSeer plugin.</param>
        public StatusPage(HSPI plugin) : base(pageName, plugin, config:true)
        {
            this.suppressDefaultFooter = true;
        }

        /// <summary>
        /// Return HTML representation of the page
        /// </summary>
        /// <param name="queryPairs">See TableBuilder.BuildHeaderLink for queryString format</param>
        /// <returns></returns>
        public override string BuildContent(NameValueCollection queryPairs = null)
        {
            var stb = new StringBuilder();

            // See TableBuilder.BuildHeaderLink for queryString format
            //BuildTable_Info(stb, queryPairs.Get("sortby"), queryPairs.Get("sortorder"));
            //this.RefreshIntervalMilliSeconds = 1000;
            //stb.Append(this.AddAjaxHandlerPost("action=updatetime", page_link));
            //
            //stb.Append("<div id='current_time'>" + "-----" + "</div>\r\n");

            stb.Append(AddTimerDiv());

            // Test Button
            stb.Append(FormButton("btn", "Test"));

            // Stop timer
            long dur = duration;

            return stb.ToString();
        }

        //private void BuildTable_Info(StringBuilder stb, string sortby = null, string sortorder = null)
        //{
        //    StatusPage.BuildTable_Info( stb,
        //                                  ((HSPI)plugin).controller.UsedDevices,
        //                                  page_name: page_link,
        //                                  sortby: sortby,
        //                                  sortorder: sortorder,
        //                                  inSlider: false
        //                                  );
        //}

        /// <summary>
        ///  Build info table for all UsedDevices
        /// </summary>
        /// <param name="stb"></param>
        /// <param name="UsedDevices"></param>
        /// <param name="title"></param>
        /// <param name="tt"></param>
        /// <param name="page_name">If want to add sorting to table columns, see AddHeader/BuildHeaderLink</param>
        /// <param name="sorthdr">on/of sorting</param>
        /// <param name="sortby">As above</param>
        /// <param name="sortorder">As above</param>
        /// <param name="inSlider">Put table inside SliderTab</param>
        /// <param name="tableID">ID for table and SliderTab</param>
        /// <param name="initiallyOpen"></param>
        /// <param name="noempty">Don't show empty table</param>
        public static void BuildTable_Info(StringBuilder stb,
                                        DeviceDict UsedDevices,
                                        string title = null,
                                        string tt = null,
                                        string page_name = null,
                                        bool sorthdr = true,
                                        string sortby = null,
                                        string sortorder = null,
                                        bool? inSlider = null,
                                        string tableID = null,
                                        bool initiallyOpen = true,
                                        bool noempty = false
                                        )
        {
            List<string> hdr = new List<string>(){ "Ref",
                PageBuilder.LocationLabels.Item1, PageBuilder.LocationLabels.Item2,
                "Name", "Plugin", "Type", "States", "Changed", "Log"};

            // Prepare TableData
            TableBuilder.TableData data = new TableBuilder.TableData();

            foreach (DeviceBase device in UsedDevices.Values)
            {
                var row = data.AddRow();

                row.Add(device.RefId.ToString());

                if (PageBuilder.bLocationFirst)
                    row.Add(device.Location);
                row.Add(device.Location2);
                if (!PageBuilder.bLocationFirst)
                    row.Add(device.Location);

                row.Add(device.GetURL());
                row.Add(device.Interface);
                row.Add(device.Type);
                row.Add(String.Join(", ", device.vspsListStr(ePairStatusControl.Both)));
                row.Add(device.LastChange);
                row.Add(BoolToImg(device.log_enable));

            }

            string html = TableBuilder.BuildTable(data,
                                                   hdr: hdr.ToArray(),
                                                   title: title,
                                                   tooltip: tt,
                                                   page_name: page_name,
                                                   width: "91%",
                                                   sorthdr: sorthdr,
                                                   sortby: sortby,
                                                   sortorder: sortorder,
                                                   noempty: noempty,
                                                   inSlider: inSlider,
                                                   tableID: tableID,
                                                   initiallyOpen: initiallyOpen
                                                   );
            stb.Append(html);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parts"></param>
        /// <returns></returns>
        public override string PostBackProc(NameValueCollection parts)
        {
            // Check which control "id" caused postback
            string ctrlID = parts.Get("id");

            if (ctrlID == null)
            {
                // ajax timer has expired and posted back to us, update the time
                CheckTimerCallback(parts, value: DateTime.Now);
            }
            else
            {
                string value = parts[ctrlID];
                Console.WriteLine($"{ctrlID}: {value}");

                if (ctrlID == "btn")
                {
                    Console.WriteLine($"{ctrlID}: {value}");
                }
            }


            return "";
        }

    }
}
